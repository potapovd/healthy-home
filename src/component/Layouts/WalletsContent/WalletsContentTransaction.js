import './WalletsContent.css';

function WalletsContentTransaction({ data }) {
  return (
    <div className="wallets-content-transaction">
      <div className="wallets-content-transaction-date-name">
        <div className="wallets-content-transaction-name">{data.name}</div>
        <div className="wallets-content-transaction-date">{data.date}</div>
      </div>
      <div className="wallets-content-transaction-value">{data.value}</div>
    </div>
  );
}

export default WalletsContentTransaction;
