import './WalletsContent.css';

import { nanoid } from 'nanoid';
import { Link } from 'react-router-dom';

import ProfileContentAside from '../ProfileContent/ProfileContentAside';
import WalletsContentPackegeItem from './WalletsContentPackegeItem';
import WalletsContentTransaction from './WalletsContentTransaction';

const packagesData = [
  {
    name: 'Basic Package',
    price: 'AED 250',
    sale: 'Save 10%',
    list: ['Upto 50 posts', 'Save 10%', 'Auto renew in six months'],
    byuNowText: 'Buy Now',
    byuNowLink: '/',
    popular: false,
  },
  {
    name: 'Smart Package',
    price: 'AED 320',
    sale: 'Save 20%',
    list: ['Upto 50+ Services ', 'Save 20%', 'Auto renew  in 12 months'],
    byuNowText: 'Buy Now',
    byuNowLink: '/',
    popular: true,
  },
  {
    name: 'Super Package',
    price: 'AED 500',
    sale: 'Save 30%',
    list: ['Unlimited Service', 'Save 30%', 'Auto renew  in 12 months'],
    byuNowText: 'Buy Now',
    byuNowLink: '/',
    popular: false,
  },
];

const transactionsData = [
  { name: 'Credit Received', date: '24 June, 2021', value: '+120.00 AED' },
  { name: 'Credit Received', date: '25 June, 2021', value: '+121.00 AED' },
  { name: 'Credit Received', date: '26 June, 2021', value: '+100.00 AED' },
  { name: 'Credit Received', date: '27 June, 2021', value: '+300.00 AED' },
  { name: 'Credit Received', date: '28 June, 2021', value: '+10.00 AED' },
];

function WalletsContent() {
  return (
    <div className="profile-content-block">
      <div className="container">
        <div className="profile-content-inner">
          <ProfileContentAside />
          <div className="profile-content-form">
            <div className="wallets-content-form-wrapper">
              <div className="wallets-content-form-name">Wallets</div>

              <div className="wallets-content-section">
                <div className="wallets-content-balance">
                  <div className="wallets-content-balance-header">Healthy Homes Credit</div>
                  <div className="wallets-content-balance-aed">AED 250</div>
                </div>

                <div className="wallets-content-wrapper">
                  <div className="wallets-content-wrapper-text-btn">
                    <div className="wallets-content-wrapper-text">
                      <div className="wallets-content-wrapper-name">Pay less & get more</div>
                      <div className="wallets-content-wrapper-text-p">
                        Assertively maintain clicks and mortar quality vectors are through maintainable sources.
                        Propriately more our maintain clicks
                      </div>
                    </div>
                    <div className="wallets-content-wrapper-btn">Top Up Credit</div>
                  </div>

                  <div className="wallets-content-wrapper-packages">
                    {packagesData.map(packageItem => (
                      <WalletsContentPackegeItem key={nanoid()} data={packageItem} />
                    ))}
                  </div>
                </div>

                <div className="wallets-content-wrapper">
                  <div className="wallets-content-wrapper-text-link">
                    <div className="wallets-content-wrapper-text">
                      <div className="wallets-content-wrapper-name">Recent Transactions</div>
                    </div>
                    <Link to="/" className="wallets-content-wrapper-link">
                      View All
                      <svg
                        className="wallets-content-wrapper-link-icon"
                        width="6"
                        height="11"
                        viewBox="0 0 6 11"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4.19194 5.24726L0.220208 1.28252C0.0792846 1.1417 7.29457e-05 0.950669 5.03567e-08 0.751446C-7.2845e-05 0.552223 0.0789984 0.361131 0.219819 0.220207C0.360639 0.0792838 0.551674 7.29458e-05 0.750897 5.03569e-08C0.95012 -7.2845e-05 1.14121 0.0789981 1.28214 0.219819L5.78173 4.7163C5.91766 4.85275 5.99578 5.03634 5.99983 5.22891C6.00389 5.42147 5.93357 5.60819 5.80349 5.75024L1.28214 10.2778C1.21256 10.3476 1.12993 10.403 1.03894 10.4408C0.947963 10.4787 0.85042 10.4982 0.751883 10.4984C0.653346 10.4985 0.555745 10.4792 0.464654 10.4417C0.373562 10.4041 0.290763 10.3489 0.220985 10.2794C0.151207 10.2098 0.0958154 10.1272 0.0579735 10.0362C0.0201316 9.9452 0.000580361 9.84766 0.000435986 9.74912C0.00029161 9.65058 0.0195569 9.55298 0.057132 9.46189C0.0947071 9.3708 0.149856 9.288 0.21943 9.21822L4.19194 5.24726Z"
                          fill="#F3A51B"
                        />
                      </svg>
                    </Link>
                  </div>

                  <div className="wallets-content-transactions">
                    {transactionsData.map(transaction => (
                      <WalletsContentTransaction key={nanoid()} data={transaction} />
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default WalletsContent;
