import { nanoid } from 'nanoid';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import ServicesListItem from './ServicesListItem';
import './ServicesList.css';

function ServicesList({ blockName, data }) {
  const responsive = {
    768: {
      items: 2,
    },
    991: {
      items: 3,
    },
    1280: {
      items: 4,
    },
  };
  const handleOnDragStart = e => e.preventDefault();
  return (
    <div className="service-list-block">
      <div className="container">
        <div className="service-list-block-name">{blockName}</div>
        <div className="service-list">
          <AliceCarousel
            fadeOutAnimation
            mouseTracking
            disableDotsControls
            infinite
            responsive={responsive}
            // autoPlayDirection="rtl"
          >
            {data.map(dataItem => (
              <ServicesListItem
                onDragStart={handleOnDragStart}
                key={nanoid()}
                image={dataItem.image}
                name={dataItem.name}
                text={dataItem.text}
              />
            ))}
          </AliceCarousel>
        </div>
      </div>
    </div>
  );
}

export default ServicesList;
