import { Link } from 'react-router-dom';
import './ServicesList.css';

function ServicesListItem({ image, name, text }) {
  return (
    <Link to="/service" className="service-item-block-simple">
      <img className="service-item-img-simple" src={image} alt={name} />
      <div className="service-item-name-simple">{name}</div>
      <div className="service-item-text-simple">{text}</div>
    </Link>
  );
}

export default ServicesListItem;
