import './ProfileContent.css';

import ProfileContentAside from './ProfileContentAside';
import ProfileContentForm from './ProfileContentForm';

function ProfileContent() {
  return (
    <div className="profile-content-block">
      <div className="container">
        <div className="profile-content-inner">
          <ProfileContentAside />
          <div className="profile-content-form">
            <ProfileContentForm />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ProfileContent;
