import React, { useState } from 'react';

import './ProfileContent.css';
import { nanoid } from 'nanoid';
import { Link } from 'react-router-dom';
import PriofileAvatar from '../../../assets/imgs/profile-avatar.png';
import InputText from '../../Form/InputText/InputText';
import Select from '../../Form/Select/Select';
import InputRadio from '../../Form/InputRadio/InputRadio';
import IconDataSection from '../../Elements/IconDataSection/IconDataSection';
import PaymentsCard from '../../Elements/PaymentsCard/PaymentsCard';
import BookingCard from '../../Elements/PopupElement/BookingCard';
import BookingAddress from '../../Elements/PopupElement/BookingAddress';

import MasterCardImage from '../../../assets/imgs/payment/master-profile.png';
import VisaImage from '../../../assets/imgs/payment/visa-profile.png';

import AddressHomeImage from '../../../assets/imgs/addess-home.png';
import AddressWorkImage from '../../../assets/imgs/addess-work.png';

const cardsData = [
  { name: 'Ashfak Sayem', image: MasterCardImage, number: '0213' },
  { name: 'Ashfak Sayem', image: VisaImage, number: '0213' },
];

const AddressData = [
  { image: AddressHomeImage, name: 'home', address: '4261 Kembery Drive, Chicago, LSA' },
  { image: AddressWorkImage, name: 'work', address: '4261 Kembery Drive, Chicago, LSA' },
];

const LanguagesList = ['English', 'Arabic', 'Hindi'];

function ProfileContentForm() {
  const [showAddress, setShowAddress] = useState(false);
  const [newCard, setNewCard] = useState(false);

  const cardDots = [];
  for (let i = 0; i < 3; i += 1) {
    cardDots.push(
      <svg
        width="26"
        height="5"
        className="card-dots"
        viewBox="0 0 26 5"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <circle cx="2.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="9.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="16.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="23.5" cy="2.5" r="2.5" fill="#434355" />
      </svg>
    );
  }

  return (
    <div className="profile-content-form-wrapper">
      <div className="profile-content-form-name">Profile</div>
      <div className="profile-content-form-inner">
        <div className="profile-content-form-inner-wpapper">
          <div className="profile-content-form-1">
            <div className="profile-content-form-1-inner">
              <img src={PriofileAvatar} className="profile-content-form-1-avatar" alt="" />
              <div className="profile-content-form-1-name">Ashfak Sayem</div>
            </div>
            <Link to="/" className="profile-content-form-1-edit">
              Edit Personal Info
            </Link>
          </div>
          <div className="profile-content-form-2">
            <div className="profile-content-form-line">
              <div className="profile-content-form-w50">
                <InputText
                  type="text"
                  id="fullname"
                  name="fullname"
                  placeholder="Full name"
                  label="Full name"
                  labelIsBlack
                  labelIsBig
                  inputIsWhite
                />
              </div>
              <div className="profile-content-form-w50">
                <InputText
                  type="phone"
                  id="phone"
                  name="phone"
                  placeholder="Phone Number"
                  label="Phone Number"
                  abelIsBlack
                  labelIsBig
                  inputIsWhite
                />
              </div>
            </div>
          </div>
          <div className="profile-content-form-3">
            <div className="profile-content-form-line">
              <div className="profile-content-form-w50">
                <InputText
                  type="mail"
                  id="mail"
                  name="mail"
                  placeholder="Email Adress"
                  label="Email Adress"
                  abelIsBlack
                  labelIsBig
                  inputIsWhite
                />
              </div>
              <div className="profile-content-form-w50">
                <InputText
                  type="date"
                  id="bday"
                  name="bday"
                  placeholder="Date of Birth"
                  label="Date of Birthr"
                  abelIsBlack
                  labelIsBig
                  inputIsWhite
                />
              </div>
            </div>
          </div>
          <div className="profile-content-form-3">
            <div className="profile-content-form-line">
              <div className="profile-content-form-w50">
                <Select
                  type="mail"
                  id="mail"
                  name="mail"
                  placeholder="Gender"
                  label="Gender"
                  abelIsBlack
                  labelIsBig
                  selectIsWhite
                  options={[
                    { name: 'male', value: 'male' },
                    { name: 'female', value: 'female' },
                  ]}
                />
              </div>
              <div className="profile-content-form-w50">
                <Select
                  type="mail"
                  id="mail"
                  name="mail"
                  placeholder="Gender"
                  label="Gender"
                  abelIsBlack
                  labelIsBig
                  selectIsWhite
                  options={[
                    { name: 'male', value: 'male' },
                    { name: 'female', value: 'female' },
                  ]}
                />
              </div>
            </div>
          </div>

          <div className="profile-content-form-title-change">
            <div className="profile-content-form-title">Questions About Me</div>
          </div>

          <div className="profile-content-form-3">
            <div className="profile-content-form-line">
              <div className="profile-content-form-w45">
                <InputRadio
                  name="History of asthma or allergies"
                  inputName="allergies"
                  checked="yes"
                  inputs={[
                    { id: 'yes-allergies', label: 'Yes' },
                    { id: 'no-allergies', label: 'No' },
                  ]}
                />
              </div>
              <div className="profile-content-form-w45">
                <InputRadio
                  name="How many kids do you have"
                  inputName="kids"
                  checked="5+"
                  inputs={[
                    { id: '1', label: '1' },
                    { id: '2', label: '2' },
                    { id: '5+', label: '5+' },
                    { id: 'no-kids', label: 'No Kids' },
                  ]}
                />
              </div>
            </div>
          </div>
          <div className="profile-content-form-4">
            <div className="profile-content-form-line">
              <div className="profile-content-form-w45">
                <InputRadio
                  name="Do you have Pets"
                  inputName="pets"
                  checked="no"
                  inputs={[
                    { id: 'yes-pets', label: 'Yes' },
                    { id: 'no-pets', label: 'No' },
                  ]}
                />
              </div>
            </div>
          </div>

          <div className="profile-content-form-title-change">
            <div className="profile-content-form-title">Manage Card</div>
            <div
              onClick={() => {
                setNewCard(!newCard);
              }}
              onKeyDown={() => {
                setNewCard(!newCard);
              }}
              role="button"
              tabIndex={-1}
              className="profile-content-form-change"
            >
              Add New Card
            </div>
          </div>

          <div className="profile-content-form-4">
            <div className="profile-content-form-line">
              {cardsData.map(card => (
                <div key={nanoid()} className="profile-content-form-w45">
                  <PaymentsCard cardData={card} />

                  <div className="profile-content-card-remove">Remove Card</div>
                </div>
              ))}
            </div>
          </div>

          <div className="profile-content-form-title-change">
            <div className="profile-content-form-title">Manage Addresses</div>
            <div
              onClick={() => {
                setShowAddress(!showAddress);
              }}
              onKeyDown={() => {
                setShowAddress(!showAddress);
              }}
              role="button"
              tabIndex={-1}
              className="profile-content-form-change"
            >
              Add/ Manage Address
            </div>
          </div>

          <div className="profile-content-form-4">
            <div className="profile-content-form-line">
              {AddressData.map(address => (
                <div key={nanoid()} className="profile-content-form-w45">
                  <IconDataSection icon={address.image} name={address.name} text={address.address} />
                </div>
              ))}
            </div>
          </div>

          <div className="profile-content-form-title-change">
            <div className="profile-content-form-title">Language Preference</div>
            <Link to="/" className="profile-content-form-change">
              Edit Language
            </Link>
          </div>

          <ul className="profile-content-language-list">
            {LanguagesList.map(language => (
              <li key={nanoid()} className="profile-content-language-list-item">
                {language}
              </li>
            ))}
          </ul>
        </div>
      </div>
      <BookingAddress open={showAddress} onCloseAction={() => setShowAddress(!showAddress)} />
      <BookingCard open={newCard} onCloseAction={() => setNewCard(!newCard)} />
    </div>
  );
}

export default ProfileContentForm;
