import './ProfileContent.css';

import { Link, useLocation } from 'react-router-dom';
import { nanoid } from 'nanoid';

const asideData = [
  { name: 'My Profile', url: '/profile', id: 'porfile' },
  { name: 'My Bookings', url: '/bookings', id: 'booking' },
  { name: 'Wallets', url: '/wallets', id: 'wallets' },
  { name: 'Package', url: '/package', id: 'package' },
];

function ProfileContentAside() {
  const location = useLocation();

  return (
    <div className="profile-content-aside">
      <ul className="profile-content-aside-widget-list">
        {asideData.map(item => (
          //
          <li
            key={nanoid()}
            className={`profile-content-aside-widget-item profile-content-aside-widget-${item.id}
               ${location.pathname === item.url && 'profile-content-aside-active'}
            `}
          >
            <Link to={item.url} className="profile-content-aside-widget-link ">
              {item.name}
            </Link>
            <div className="profile-content-aside-widget-arrow">
              <svg width="10" height="15" viewBox="0 0 10 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M1.365 0.957094L7.83008 8.1541L0.999927 14" strokeWidth="2" />
              </svg>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default ProfileContentAside;
