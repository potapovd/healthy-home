import './SupportContacts.css';

import { Link } from 'react-router-dom';

function SupportContactItem({ dataItem }) {
  return (
    <div className="support-contact-item">
      <img src={dataItem.image} className="support-contact-item-icon" alt="" />
      <div className="support-contact-item-name">{dataItem.name}</div>
      <div
        className="support-contact-item-text"
        // eslint-disable-next-line react/no-danger
        dangerouslySetInnerHTML={{ __html: dataItem.text }}
      />
      <Link to="/" className="support-contact-item-link">
        <div className="support-contact-item-link-text">{dataItem.linkName}</div>
        <svg
          width="17"
          height="14"
          className="support-contact-item-link-arrow"
          viewBox="0 0 17 14"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M16.081 7.66247L10.2922 13.4747C10.1062 13.6607 9.87373 13.7537 9.64124 13.7537C9.40876 13.7537 9.17628 13.6607 8.99029 13.4747C8.61832 13.1027 8.61832 12.5215 8.99029 12.1495L13.1982 7.94146H0.929932C0.41847 7.94146 0 7.52298 0 7.0115C0 6.50003 0.41847 6.08155 0.929932 6.08155H13.1982L8.96704 1.85026C8.59507 1.47828 8.59507 0.897061 8.96704 0.52508C9.33902 0.153098 9.92022 0.153098 10.2922 0.52508L16.081 6.33729C16.453 6.70927 16.453 7.31374 16.081 7.66247Z"
            fill="#F4A418"
          />
        </svg>
      </Link>
    </div>
  );
}

export default SupportContactItem;
