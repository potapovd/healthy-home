import './SupportContacts.css';
import { nanoid } from 'nanoid';
import SupportContactItem from './SupportContactItem';

function SupportContacts({ data }) {
  return (
    <div className="support-contacts-block">
      <div className="container">
        <div className="support-contacts-inner">
          {data.map(dataItem => (
            <SupportContactItem key={nanoid()} dataItem={dataItem} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default SupportContacts;
