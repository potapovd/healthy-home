import './Login.css';

import { Link } from 'react-router-dom';

import InputText from '../../Form/InputText/InputText';
import IntlTelInputSelect from '../../Form/IntlTelInputSelect/IntlTelInputSelect';
import SelectInputText from '../../Form/SelectInputText/SelectInputText';

function SignUpContent() {
  return (
    <div className="login-content-block">
      <div className="container">
        <div className="login-content-block-section">
          <div className="login-content-name">Sign up your account</div>
          <div className="login-content-name-line" />

          <div className="login-content-names">
            <div className="login-content-name-field">
              <SelectInputText
                idInput="firstname"
                idSelect="sex"
                nameSelect="sex"
                placeholderInput="First Name"
                label="First Name"
                labelIsUPppercase
                labelIsBlack
                labelHasSpace
                inputIsWhite
                options={[
                  { name: 'Mr.', value: 'mr' },
                  { name: 'Mrs.', value: 'mrs' },
                ]}
              />
            </div>
            <div className="login-content-name-field">
              <InputText
                type="lastname"
                id="lastname"
                name="lastname"
                placeholder="Last Name"
                label="Last Name"
                labelIsUPppercase
                labelIsBlack
                labelHasSpace
                inputIsWhite
              />
            </div>
          </div>

          <IntlTelInputSelect
            placeholder="Enter you phone number"
            label="Phone Number"
            name="phone"
            id="phone"
            labelIsUPppercase
            labelIsBlack
            labelHasSpace
            inputIsWhite
          />

          <InputText
            type="email"
            id="email"
            name="email"
            placeholder="Email"
            label="Email"
            labelIsUPppercase
            labelIsBlack
            labelHasSpace
            inputIsWhite
          />

          <button type="button" className="login-content-btn-submit">
            Signup
          </button>

          <div className="container-small">
            <div className="container-small-remember">
              Already have an account? <Link to="/sign-in">Signin</Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignUpContent;
