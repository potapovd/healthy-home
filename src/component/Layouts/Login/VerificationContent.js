import './Login.css';

import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import ReactCodeInput from 'react-verification-code-input';

function VerificationContent() {
  const [seconds, setSeconds] = useState(20);
  const [done, setDone] = useState(false);
  useEffect(() => {
    if (seconds > 0) {
      setTimeout(() => setSeconds(seconds - 1), 1000);
    } else {
      setSeconds(0);
      setDone(true);
    }
  });
  return (
    <div className="login-content-block">
      <div className="container">
        <div className="login-content-block-section">
          <div className="login-content-name">Verification</div>
          <div className="login-content-name-line" />

          <div className="login-content-info">We’ve send you the verification code on ********** 7631</div>

          <div className="login-content-numbers">
            <ReactCodeInput fields={4} className="verification-field-fix" placeholder={['-', '-', '-', '-']} />
          </div>

          <button type="button" className="login-content-btn-submit">
            Continue
          </button>

          <div className="container-small">
            <div className="container-small-remember">
              Re-send code in &nbsp;
              {seconds ? `${seconds} seconds` : ''}
              {done && (
                <Link className="container-small-color" to="/">
                  Resend
                </Link>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default VerificationContent;
