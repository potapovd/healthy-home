import './Login.css';

import { Link } from 'react-router-dom';

import IntlTelInputSelect from '../../Form/IntlTelInputSelect/IntlTelInputSelect';

function SignInContent() {
  return (
    <div className="login-content-block">
      <div className="container">
        <div className="login-content-block-section">
          <div className="login-content-name">Sign in your account</div>
          <div className="login-content-name-line" />

          <IntlTelInputSelect
            placeholder="Enter you phone number"
            label="Phone Number"
            name="phone"
            id="phone"
            labelIsUPppercase
            labelIsBlack
            labelHasSpace
            inputIsWhite
          />

          <button type="button" className="login-content-btn-submit">
            Sign in
          </button>

          <div className="container-small">
            <div className="container-small-name">
              <div className="container-small-name-line" />
              <div className="container-small-name-wrapper">sign in with</div>
            </div>

            <div className="container-small-btns">
              <button type="button" className="container-small-btn">
                <svg
                  className="container-small-btn-icon"
                  width="32"
                  height="31"
                  viewBox="0 0 32 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect x="0.494141" width="30.7551" height="30.7551" rx="15.3776" fill="#1977F3" />
                  <path
                    fillRule="evenodd"
                    clipRule="evenodd"
                    d="M18.5908 30.5153C17.7083 30.6727 16.7997 30.7549 15.8719 30.7549C15.0495 30.7549 14.2423 30.6904 13.4549 30.566V20.276H9.28174V15.5407H13.4549V11.9316C13.4549 7.82705 15.9071 5.55908 19.6625 5.55908C20.895 5.57625 22.1245 5.68323 23.3412 5.87918V9.91075H21.2677C19.2263 9.91075 18.5908 11.1734 18.5908 12.4701V15.5414H23.1488L22.4202 20.276H18.5908V30.5153Z"
                    fill="#FFFBFB"
                  />
                </svg>
                <div className="container-small-btn-name">Facebook</div>
              </button>

              <button type="button" className="container-small-btn">
                <svg
                  className="container-small-btn-icon"
                  width="32"
                  height="31"
                  viewBox="0 0 32 31"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M15.8139 5.90877C18.0033 5.87509 20.1206 6.69074 21.7215 8.18456L26.0336 3.96796C23.2671 1.37562 19.6049 -0.0458711 15.8139 0.00112944C12.9814 0.000474937 10.2045 0.787791 7.79383 2.27503C5.38316 3.76227 3.43374 5.8908 2.16357 8.42257L7.10449 12.2586C7.70864 10.4216 8.87419 8.82063 10.4368 7.68138C11.9994 6.54213 13.8801 5.92211 15.8139 5.90877Z"
                    fill="#E43E2B"
                  />
                  <path
                    d="M30.4819 15.6206C30.4999 14.5702 30.3914 13.5215 30.1587 12.4971H15.8135V18.1682H24.2349C24.0752 19.1624 23.716 20.1141 23.1789 20.966C22.6419 21.8179 21.9381 22.5524 21.1099 23.1252L25.9318 26.86C27.4346 25.4091 28.6164 23.6592 29.4009 21.7232C30.1854 19.7873 30.5552 17.7083 30.4864 15.6206H30.4819Z"
                    fill="#3B7DED"
                  />
                  <path
                    d="M7.12208 18.3027C6.78801 17.33 6.61578 16.3091 6.61227 15.2806C6.61836 14.2538 6.78441 13.2341 7.10445 12.2584L2.16354 8.42236C1.09189 10.55 0.533691 12.8991 0.533691 15.2813C0.533691 17.6636 1.09189 20.0127 2.16354 22.1403L7.12208 18.3027Z"
                    fill="#F0B501"
                  />
                  <path
                    d="M15.8138 30.5603C19.5357 30.6655 23.1566 29.3411 25.9322 26.8593L21.1103 23.1246C19.5469 24.1728 17.6953 24.7069 15.8138 24.6526C13.8817 24.6411 12.0023 24.0215 10.4422 22.8818C8.88204 21.742 7.7204 20.1399 7.12207 18.3027L2.18115 22.1403C3.44789 24.6709 5.39412 26.799 7.80188 28.2861C10.2096 29.7732 12.9839 30.5606 15.8138 30.5603Z"
                    fill="#2BA24C"
                  />
                </svg>

                <div className="container-small-btn-name">Google</div>
              </button>
            </div>

            <div className="container-small-remember">
              Don’t have an account?
              <Link to="/sign-up"> Sign up </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignInContent;
