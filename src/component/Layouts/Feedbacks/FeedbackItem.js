import './Feedbacks.css';

function FeedbackItem({ text, name, job, image }) {
  return (
    <div className="feedback-item-out">
      {/* TODO - CHANGE TO LINK */}
      <div className="feedback-item-text">{text}</div>
      <div className="feedback-item-account">
        <img className="feedback-item-account-img" src={image} alt="" />
        <div className="feedback-item-account-info">
          <div className="feedback-item-account-name">{name}</div>
          <div className="feedback-item-account-job">{job}</div>
        </div>
      </div>
    </div>
  );
}

export default FeedbackItem;
