import './Feedbacks.css';
import { nanoid } from 'nanoid';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

import FeedbackItem from './FeedbackItem';

function Feedbacks({ blockName, data }) {
  const responsive = {
    768: {
      items: 2,
    },
    991: {
      items: 3,
    },
    1280: {
      items: 4,
    },
    1500: {
      items: 5,
    },
  };
  return (
    <div className="feedback-block">
      <div className="container">
        <div className="feedback-name-block">{blockName}</div>
        <div className="feedback-block-cards">
          <AliceCarousel
            fadeOutAnimation
            mouseTracking
            disableDotsControls
            infinite
            responsive={responsive}
            // autoPlayDirection="rtl"
          >
            {data.map(dataItem => (
              <FeedbackItem
                key={nanoid()}
                text={dataItem.text}
                name={dataItem.name}
                job={dataItem.job}
                image={dataItem.image}
              />
            ))}
          </AliceCarousel>
        </div>
      </div>
    </div>
  );
}

export default Feedbacks;
