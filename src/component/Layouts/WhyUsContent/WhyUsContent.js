import './WhyUsContent.css';

function WhyUsContent({ text }) {
  return (
    <div className="why-us-content-block">
      <div className="container">
        <div className="why-us-content-block-wrapper">
          <div
            className="why-us-content-block-main"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: text }}
          />
        </div>
      </div>
    </div>
  );
}

export default WhyUsContent;
