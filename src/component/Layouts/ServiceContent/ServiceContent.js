import './ServiceContent.css';

import ButtonLink from '../../Form/Button/ButtonLink';

function ServiceContent() {
  return (
    <div className="service-content-block">
      <div className="container">
        <div className="service-content-inner">
          <h2 className="service-content-header">Home Cleaning Services are Just a Few Clicks Away</h2>
          <div className="service-content-subheader">Home Cleaning from Healthy Homes</div>
          <div className="service-content-main">
            <p>
              Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your house
              appear clean from outside, there are certain areas where dust and dirt accumulate affecting the overall
              hygiene and look of the house. It is advisable to deep clean the house once in 2-3 months, so that the
              house remain bright, fresh and hygienic throughout.
            </p>
            <p>
              With the help of our professional cleaning staff, Justmop helps in keeping your house neat and hygienic.
              We select our cleaning staff after a thorough background check and provide rigorous training to them
              periodically. This ensures that only the cream of the industry get to work through us and our customers
              get a supreme cleaning service. We undertake the following home cleaning services
            </p>
            <ul>
              <li>
                <b>General cleaning:</b> We undertake dusting, vacuuming and mopping of all rooms of the house as per
                your requirement.
              </li>
              <li>
                <b>General cleaning:</b> We undertake dusting, vacuuming and mopping of all rooms of the house as per
                your requirement.
              </li>
              <li>
                <b>General cleaning:</b> We undertake dusting, vacuuming and mopping of all rooms of the house as per
                your requirement.
              </li>
              <li>
                <b>General cleaning:</b> We undertake dusting, vacuuming and mopping of all rooms of the house as per
                your requirement.
              </li>
            </ul>
            <div className="service-content-main-btn">
              <ButtonLink url="booking/ac" label="Book Now" fontSize={18} size="l" />
            </div>

            {/* <Link to="booking/ac" className="service-content-main-btn">
              Book Now
            </Link> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ServiceContent;
