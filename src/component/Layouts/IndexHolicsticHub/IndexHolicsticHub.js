import './IndexHolicsticHub.css';

function IndexHolicsticHub() {
  return (
    <div className="holistic-block">
      <div className="container">
        <div className="holistic-name-block">Your Holistic Wellness Hub</div>
        <div className="holistic-text-block">
          We aim to provide a holistic direction by spreading awareness on the importance of a better and healthier
          lifestyle at home and beyond.
        </div>
      </div>
      <div className="container">
        <div className="holistic-block-100">
          <div className="holistic-block-text">Your health starts from your indoor air quality</div>
        </div>
        <div className="holistic-block-line">
          <div className="holistic-block-50 holistic-block-text-right">
            <div className="holistic-block-text">Reach your fitness goals with us</div>
          </div>
          <div className="holistic-block-50 holistic-block-text-left">
            <div className="holistic-block-text">Reach your fitness goals with us</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default IndexHolicsticHub;
