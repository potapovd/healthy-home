import { Link } from 'react-router-dom';
import './ExpertsPanel.css';

function ExpertsPanelItem({ dataItem }) {
  return (
    <div className="experts-panel-item">
      <img src={dataItem.image} className="experts-panel-item-photo" alt="" />
      <div className="experts-panel-item-name">{dataItem.name}</div>
      <div className="experts-panel-item-job">{dataItem.job}</div>
      <div className="experts-panel-item-text">
        {dataItem.text}
        <Link to="/" className="experts-panel-item-text-link">
          More
        </Link>
      </div>
    </div>
  );
}

export default ExpertsPanelItem;
