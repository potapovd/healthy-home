import './ExpertsPanel.css';
import { nanoid } from 'nanoid';
import { Link } from 'react-router-dom';
import ExpertsPanelItem from './ExpertsPanelItem';

function ExpertsPanel({ data }) {
  return (
    <div className="experts-panel-block">
      <div className="container">
        <div className="experts-panel-name">Our Expert Panel</div>
        <div className="experts-panel-text">
          To connect in that most indescribable way. Somehow, they do it every day.
        </div>
        <div className="experts-panel-tags-search">
          <div className="experts-panel-tags">
            <Link to="/" className="experts-panel-tag  experts-panel-tag-active">
              All
            </Link>
            <Link to="/" className="experts-panel-tag">
              Life Style
            </Link>
            <Link to="/" className="experts-panel-tag">
              Tag Name
            </Link>
            <Link to="/" className="experts-panel-tag">
              Tag Name
            </Link>
            <Link to="/" className="experts-panel-tag">
              Life Style
            </Link>
            <Link to="/" className="experts-panel-tag">
              Tag Name
            </Link>
            <Link to="/" className="experts-panel-tag">
              Tag Name
            </Link>
            <Link to="/" className="experts-panel-tag">
              Tag Name
            </Link>
          </div>
          <div className="experts-panel-search">
            <div className="input-field experts-input-field">
              <input
                type="serach"
                name="search"
                id="search"
                className="input-field-text experts-input-field-text"
                placeholder="search here"
              />
              <svg
                width="20"
                height="20"
                className="experts-input-icon"
                viewBox="0 0 20 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M19.5264 17.1065L14.9282 12.5081C15.7095 11.2643 16.1629 9.79421 16.1629 8.21655C16.1629 3.75311 12.5446 0.135254 8.08129 0.135254C3.61803 0.135254 0 3.75311 0 8.21655C0 12.6801 3.61786 16.2977 8.08129 16.2977C9.7982 16.2977 11.3885 15.7609 12.6971 14.8488L17.2405 19.3926C17.5562 19.7079 17.9701 19.865 18.3834 19.865C18.7973 19.865 19.2107 19.7079 19.5269 19.3926C20.1578 18.761 20.1578 17.7379 19.5264 17.1065ZM8.08129 13.6794C5.06453 13.6794 2.61873 11.2338 2.61873 8.21688C2.61873 5.19995 5.06453 2.75415 8.08129 2.75415C11.0982 2.75415 13.5439 5.19995 13.5439 8.21688C13.5439 11.2338 11.0982 13.6794 8.08129 13.6794Z"
                  fill="#D6D6D6"
                />
              </svg>
            </div>
          </div>
        </div>
        <div className="experts-panel-items">
          {data.map(item => (
            <ExpertsPanelItem key={nanoid()} dataItem={item} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default ExpertsPanel;
