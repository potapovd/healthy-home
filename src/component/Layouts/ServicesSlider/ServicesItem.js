import './ServicesSlider.css';

import { Link } from 'react-router-dom';

import ButtonLink from '../../Form/Button/ButtonLink';

function ServicesItem({ image, name, text, url, booking }) {
  return (
    <div className="service-item">
      <img src={image} alt={name} className="service-item-img" />
      <div className="service-item-block">
        <Link to={url}>
          <div className="service-item-name">{name}</div>
          <div className="service-item-text">{text}</div>
        </Link>
        <div className="service-item-booking">
          <ButtonLink label="Book" url={booking} />
        </div>
      </div>
    </div>
  );
}

export default ServicesItem;
