import 'react-alice-carousel/lib/alice-carousel.css';
import './ServicesSlider.css';

import { nanoid } from 'nanoid';
import AliceCarousel from 'react-alice-carousel';

import ServicesItem from './ServicesItem';

function ServicesSlider({ blockName, data }) {
  const responsive = {
    520: {
      items: 2,
    },
    767: {
      items: 3,
    },
    991: {
      items: 4,
    },
    1023: {
      items: 5,
    },
  };
  const handleOnDragStart = e => e.preventDefault();
  return (
    <div className="services-slider-block">
      <div className="container">
        <div className="services-slider-name">{blockName}</div>

        <div className="services-list-items">
          <AliceCarousel
            fadeOutAnimation
            mouseTracking
            disableDotsControls
            infinite
            responsive={responsive}
            // autoPlayDirection="rtl"
          >
            {data.map(dataItem => (
              <ServicesItem
                key={nanoid()}
                onDragStart={handleOnDragStart}
                image={dataItem.image}
                name={dataItem.name}
                text={dataItem.text}
                booking={dataItem.booking}
                url={dataItem.url}
              />
            ))}
          </AliceCarousel>
        </div>
      </div>
    </div>
  );
}

export default ServicesSlider;
