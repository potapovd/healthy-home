import './SupportDropMessage.css';
import { nanoid } from 'nanoid';
import SupportDropMessageForm from './SupportDropMessageForm';
import SupportDropMessageOffice from './SupportDropMessageOffice';

function SupportDropMessage({ data }) {
  return (
    <div className="support-drop-message-block">
      <div className="container ">
        <div className="support-drop-message-block-name">Drop us a Message</div>
        <div className="support-drop-message-block-inner">
          <SupportDropMessageForm />

          <div className="support-drop-message-block-offices">
            {data.map(itemOffice => (
              <SupportDropMessageOffice key={nanoid()} item={itemOffice} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SupportDropMessage;
