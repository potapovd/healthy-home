import './SupportDropMessage.css';

import ButtonAction from '../../Form/Button/ButtonAction';
import InputText from '../../Form/InputText/InputText';
import Textarea from '../../Form/Textarea/Textarea';

function SubmitForm() {
  // eslint-disable-next-line no-alert
  alert('submit!');
}

function SupportDropMessageForm() {
  return (
    <div className="support-drop-message-form-block">
      <form action="" method="get">
        <div className="support-drop-message-form-line">
          <div className="support-drop-message-form-line-50">
            <InputText
              type="text"
              id="firsname"
              name="firsname"
              placeholder="First Name"
              label="First Name"
              labelIsUppercase
            />
          </div>
          <div className="support-drop-message-form-line-50">
            <InputText
              itype="text"
              d="lastname"
              name="lastname"
              placeholder="Last Name"
              label="Last Name"
              labelIsUppercase
            />
          </div>
        </div>
        <div className="support-drop-message-form-line">
          <div className="support-drop-message-form-line-50">
            <InputText
              type="phone"
              id="phone"
              name="phone"
              placeholder="Phone Number"
              label="Phone Number"
              labelIsUppercase
            />
          </div>
          <div className="support-drop-message-form-line-50">
            <InputText type="mail" id="mail" name="mail" placeholder="Email" label="Email" labelIsUppercase />
          </div>
        </div>
        <div className="support-drop-message-form-line">
          <div className="support-drop-message-form-line-100">
            <Textarea id="message" name="message" placeholder="Write your message" label="Message" labelIsUppercase />
          </div>
        </div>
        <div className="support-drop-message-form-line">
          <div className="support-drop-message-form-line-100">
            <ButtonAction label="Submit Now!" isUppercase size="m" onClickAction={SubmitForm} />
          </div>
        </div>
      </form>
    </div>
  );
}

export default SupportDropMessageForm;
