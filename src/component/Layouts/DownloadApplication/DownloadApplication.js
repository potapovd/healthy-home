import './DownloadApplication.css';

import Device from '../../../assets/imgs/device.png';
import Apple from '../../../assets/imgs/btn-apple-bg.png';
import Andorid from '../../../assets/imgs/btn-android-bg.png';
import DownloadApplicationButton from './DownloadApplicationButton';

function DownloadApplication() {
  return (
    <div className="download-block">
      <div className="container download-block-inner">
        <img src={Device} alt="" className="download-image" />
        <div className="download-text">
          <div className="download-text1">Download our app</div>
          <div className="download-text2">
            Book your home sanitization and disinfection service in Dubai with just a few clicks. Explore our services
            and more anywhere you go!
          </div>
          <div className="download-btns">
            <DownloadApplicationButton image={Apple} text="App Store" link="/" />
            <DownloadApplicationButton image={Andorid} text="Play Store" link="/" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default DownloadApplication;
