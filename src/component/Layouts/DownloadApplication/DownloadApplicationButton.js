import { Link } from 'react-router-dom';
import './DownloadApplication.css';

function DownloadApplicationButton({ image, text, link }) {
  return (
    <Link to={link} className="download-btn">
      <img src={image} alt={link} className="btn-img" />
      <div className="btn-text">
        <div className="btn-text1">Download on the</div>
        <div className="btn-text2">{text}</div>
      </div>
    </Link>
  );
}

export default DownloadApplicationButton;
