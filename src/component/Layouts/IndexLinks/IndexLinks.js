import { Link } from 'react-router-dom';
import './IndexLinks.css';

function IndexLinks() {
  return (
    <div className="links-block">
      <div className="container-footer">
        {/* TODO - LINKS */}
        <nav className="links-block-inner">
          <div className="links-block-col">
            <Link to="service" className="links-block-name">
              AC Services
            </Link>
            <Link to="/service" className="links-block-link">
              AC Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              AC Duct Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              AC Maintenance
            </Link>
            <Link to="/service" className="links-block-link">
              AC Repair
            </Link>
            <Link to="/service" className="links-block-link">
              AC Coil Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              AC installation
            </Link>
          </div>
          <div className="links-block-col">
            <Link to="service" className="links-block-name">
              Cleaning Services
            </Link>
            <Link to="/service" className="links-block-link">
              Mattress Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              Carpet Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              Sofa Cleaning Dubai
            </Link>
            <Link to="/service" className="links-block-link">
              Curtain Cleaning Dubai
            </Link>
            <Link to="/service" className="links-block-link">
              Move-in Deep Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              AC installation
            </Link>
            <Link to="/service" className="links-block-link">
              Deep Cleaning Dubai
            </Link>
            <Link to="/service" className="links-block-link">
              Water Tank Cleaning
            </Link>
            <Link to="/service" className="links-block-link">
              Furniture Cleaning
            </Link>
          </div>
          <div className="links-block-col">
            <Link to="service" className="links-block-name">
              Pest Control Services
            </Link>
            <Link to="/service" className="links-block-link">
              Pest Control in Dubai
            </Link>
            <Link to="/service" className="links-block-link">
              Pest Control in Abu Dhabi
            </Link>
            <Link to="/service" className="links-block-link">
              Ants Pest control
            </Link>
            <Link to="/service" className="links-block-link">
              Bed bugs Pest Control
            </Link>
            <Link to="/service" className="links-block-link">
              Cockroach Pest Control
            </Link>
            <Link to="/service" className="links-block-link">
              Rodents Pest Control
            </Link>
          </div>
          <div className="links-block-col">
            <Link to="service" className="links-block-name">
              Disinfection & Sanitization
            </Link>
            <Link to="/service" className="links-block-link">
              Home Disinfection & Sanitization
            </Link>
            <Link to="/service" className="links-block-link">
              Office Disinfection & Sanitization
            </Link>
            <Link to="/service" className="links-block-link">
              Car Disinfection & Sanitization
            </Link>
            <Link to="/service" className="links-block-link">
              Transportation Disinfection & Sanitization
            </Link>
            <Link to="/service" className="links-block-link">
              Covid-19 Disinfection
            </Link>
          </div>
        </nav>
      </div>
    </div>
  );
}

export default IndexLinks;
