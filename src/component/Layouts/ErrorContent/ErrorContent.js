import './ErrorContent.css';

function ErrorContent() {
  return (
    <div className="error-content-block">
      <div className="container">
        <div className="error-subheader">404 Error</div>
        <div className="error-header">We are doing our best to fix it.</div>
        <div className="error-secription">
          Cepteur occaecat cupidatat proident, taken possession of my entire soul, Asertively maintain click and mortar
          quality vectors through next-generation technologies like these sweet mornings of spring which.
        </div>
      </div>
    </div>
  );
}

export default ErrorContent;
