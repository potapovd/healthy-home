import './NewsCategoriesContent.css';

import { Link } from 'react-router-dom';

import BannerImg from '../../../assets/imgs/news/banner.jpg';
import NewsCategoriesImage1 from '../../../assets/imgs/news/news-1.png';
import NewsCategoriesImage2 from '../../../assets/imgs/news/news-2.png';
import NewsCategoriesImage3 from '../../../assets/imgs/news/news-3.png';
import NewsCategoriesImage4 from '../../../assets/imgs/news/news-4.png';
import NewsCategoriesImage5 from '../../../assets/imgs/news/news-5.png';
import NewsCategoriesImage6 from '../../../assets/imgs/news/news-6.png';
import NewsCategoriesContentItem from './NewsCategoriesContentItem';

function Search() {}
function NewsCategoriesContent() {
  return (
    <div className="news-categories-block">
      <div className="container">
        <div className="news-categories-inner">
          <div className="news-categories-aside">
            <div className="news-categories-aside-search">
              <input
                id="search-input"
                type="search"
                placeholder="Type your keyword"
                className="news-categories-aside-search-input"
              />
              <button
                type="button"
                onClick={() => Search}
                onKeyDown={() => Search}
                className="news-categories-aside-search-button"
              >
                <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <circle
                    cx="8.16667"
                    cy="8.16667"
                    r="6.66667"
                    stroke="white"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                  <path
                    d="M16.5 16.5L12.875 12.875"
                    stroke="white"
                    strokeWidth="2"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                  />
                </svg>
              </button>
            </div>

            <div className="news-categories-aside-widget">
              <div className="news-categories-aside-widget-name">Categories</div>
              <ul className="news-categories-aside-widget-list">
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link news-categories-aside-active">
                    General
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Facts & Stats
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Diseases
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Solutions
                  </Link>
                </li>
              </ul>
            </div>

            <div className="news-categories-aside-widget">
              <div className="news-categories-aside-widget-name">Services</div>
              <ul className="news-categories-aside-widget-list">
                {/* TODO - LINK */}
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link news-categories-aside-active">
                    AC Cleaning
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Mattress
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Pest Control
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Curtain
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Carpet
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Disinfection
                  </Link>
                </li>
                <li className="news-categories-aside-widget-item">
                  <Link to="/" className="news-categories-aside-widget-link">
                    Water tank
                  </Link>
                </li>
              </ul>
            </div>
            <div className="news-categories-aside-banner">
              <img src={BannerImg} className="news-categories-aside-banner-img" alt="" />
            </div>
          </div>
          <div className="news-categories-content">
            <NewsCategoriesContentItem
              image={NewsCategoriesImage1}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
            <NewsCategoriesContentItem
              image={NewsCategoriesImage2}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
            <NewsCategoriesContentItem
              image={NewsCategoriesImage3}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
            <NewsCategoriesContentItem
              image={NewsCategoriesImage4}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
            <NewsCategoriesContentItem
              image={NewsCategoriesImage5}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
            <NewsCategoriesContentItem
              image={NewsCategoriesImage6}
              name="Private Contemporary Home Balancing Openness"
              date="20 th June 2021"
              text="Maintaining a house spic and span is a cumbersome task. Though regular dusting and moping make your"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default NewsCategoriesContent;
