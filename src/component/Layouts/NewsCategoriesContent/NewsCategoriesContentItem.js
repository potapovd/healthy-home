import './NewsCategoriesContent.css';

function NewsCategoriesContentItem({ image, name, date, text }) {
  return (
    <div className="news-categories-item">
      <img src={image} className="news-categories-item-image" alt="" />
      <div className="news-categories-item-date">{date}</div>
      <div className="news-categories-item-name">{name}</div>
      <div className="news-categories-item-text">{text}</div>
    </div>
  );
}

export default NewsCategoriesContentItem;
