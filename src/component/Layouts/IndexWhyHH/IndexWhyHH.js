import './IndexWhyHH.css';

import IndexWhyHHItem from './IndexWhyHHItem';

import why1 from '../../../assets/imgs/whyhh/why1.png';
import why2 from '../../../assets/imgs/whyhh/why2.png';
import why3 from '../../../assets/imgs/whyhh/why3.png';
import why4 from '../../../assets/imgs/whyhh/why4.png';
import why5 from '../../../assets/imgs/whyhh/why5.png';

function IndexWhyHH() {
  return (
    <div className="why-block">
      <div className="container">
        <div className="why-name-block">Why The Healthy Home</div>
        <div className="why-block-cards">
          <IndexWhyHHItem
            image={why1}
            name="Eco-Friendly "
            text="Our chemical-free and eco-friendly solutions ensure the safety of your family"
          />
          <IndexWhyHHItem
            image={why2}
            name="Improved Lifestyle"
            text="Our treatments aim to improve your quality of sleep and beathing"
          />
          <IndexWhyHHItem
            image={why3}
            name="Eco-Friendly "
            text="Our chemical-free and eco-friendly solutions ensure the safety of your family"
          />
          <IndexWhyHHItem
            image={why4}
            name="Eco-Friendly "
            text="Our chemical-free and eco-friendly solutions ensure the safety of your family"
          />
          <IndexWhyHHItem
            image={why5}
            name="Eco-Friendly "
            text="Our chemical-free and eco-friendly solutions ensure the safety of your family"
          />
        </div>
      </div>
    </div>
  );
}

export default IndexWhyHH;
