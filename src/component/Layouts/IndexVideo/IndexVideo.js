import './IndexVideo.css';

import VideoPlayer from '../../Elements/VideoPlayer/VideoPlayer';
import ButtonLink from '../../Form/Button/ButtonLink';

function IndexVideo({ data }) {
  return (
    <div className="video-block">
      <div className="container video-block-out">
        <div className="video-block-text">
          <div className="video-block-text1"> Creating Healthy Living </div>
          <div className="video-block-text2"> We care about every detail in your home as much as you do ! </div>
          <ButtonLink label="Learn More" url="/" isOutline />
        </div>
        <div className="video-block-video">
          <VideoPlayer data={data} />
        </div>
      </div>
    </div>
  );
}

export default IndexVideo;
