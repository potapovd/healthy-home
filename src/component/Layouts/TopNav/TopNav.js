import './TopNav.css';

import React, { useState } from 'react';

import { Link } from 'react-router-dom';

import Avatar from '../../../assets/imgs/avatar.png';
import Flag from '../../../assets/imgs/flag-uae.png';
import LogoLight from '../../../assets/imgs/logo-light.svg';
import Logo from '../../../assets/imgs/logo.svg';

function isLoggedInMenuItem(AvatarImg) {
  return (
    <li className="has-sub-menu top-menu-flag-list">
      <img className="top-menu-flag" src={AvatarImg} alt="" />
      <Link to="/">Ashfak</Link>
      <ul className="top-submenu top-submenu-short">
        <li>
          <Link to="/">Item1</Link>
        </li>
        <li>
          <Link to="/">Item2</Link>
        </li>
      </ul>
    </li>
  );
}

function TopNav({ isLight, isLoggedIn, whiteBackgroud }) {
  const [isOpen, setIsOpen] = useState(0);
  const onClick = () => setIsOpen(!isOpen);
  return (
    <div className={`${isLight ? 'top-nav-light' : ''} ${whiteBackgroud ? 'top-nav-white-bg' : ''} `}>
      <div className="container top-nav">
        <Link to="/">{isLight ? <img src={LogoLight} alt="logo" /> : <img src={Logo} alt="logo" />}</Link>

        <nav role="navigation">
          <ul className={`top-menu ${isOpen && 'top-menu-mobile-open'}`}>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li className="has-sub-menu">
              <Link to="/services">Services</Link>

              <ul className="top-submenu">
                <li>
                  <Link to="/">Pure Air</Link>
                </li>
                <li>
                  <Link to="/">Pure Sleep </Link>
                </li>
                <li>
                  <Link to="/">Pure Living </Link>
                </li>
                <li>
                  <Link to="/">Safe Pest Control</Link>
                </li>
                <li>
                  <Link to="/">Pure Water </Link>
                </li>
                <li>
                  <Link to="/">Safe Shield</Link>
                </li>
              </ul>
            </li>
            <li>
              <Link to="/">Shop</Link>
            </li>
            <li>
              <Link to="/">Explore</Link>
            </li>
            <li>
              <Link to="/support">Contact</Link>
            </li>
            <li className="top-menu-item-group-start">
              <Link to="/sign-in">Login </Link>
            </li>
            <li className="top-menu-item-group-end">
              <Link to="/sign-up">Sign up </Link>
            </li>

            {isLoggedIn && isLoggedInMenuItem(Avatar)}
            <li className="has-sub-menu top-menu-flag-list">
              <img className="top-menu-flag" src={Flag} alt="" />
              <Link to="/">UAE</Link>
              <ul className="top-submenu top-submenu-short">
                <li>
                  <Link to="/">Item1</Link>
                </li>
                <li>
                  <Link to="/">Item2</Link>
                </li>
              </ul>
            </li>

            <li className="has-sub-menu">
              <Link to="/">EN</Link>
              <ul className="top-submenu top-submenu-short">
                <li>
                  <Link to="/">DE</Link>
                </li>
                <li>
                  <Link to="/">NL</Link>
                </li>
                <li>
                  <Link to="/">RU</Link>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <div className="top-menu-mobile-navigation">
          <div
            role="button"
            tabIndex={0}
            id="top-menu-mobile-navigation-hamburger"
            onClick={onClick}
            onKeyDown={onClick}
            className={isOpen && 'hamburger-open'}
          >
            <span />
            <span />
            <span />
          </div>
        </div>
      </div>
    </div>
  );
}

export default TopNav;
