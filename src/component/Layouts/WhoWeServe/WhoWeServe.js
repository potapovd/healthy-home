import './WhoWeServe.css';
import { nanoid } from 'nanoid';
import WhoWeServeItem from './WhoWeServeItem';

function WhoWeServe({ data }) {
  return (
    <div className="who-we-werve-block">
      <div className="container">
        <div className="who-we-werve-name">Who We Serve</div>
        <div className="who-we-werve-items">
          {data.map(item => (
            <WhoWeServeItem key={nanoid()} image={item.image} name={item.name} text={item.text} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default WhoWeServe;
