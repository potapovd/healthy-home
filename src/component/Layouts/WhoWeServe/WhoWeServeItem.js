import './WhoWeServe.css';

function WhoWeServeItem({ image, name, text }) {
  return (
    <div className="who-we-serve-item-block">
      <img src={image} className="who-we-serve-item-image" alt="" />
      <div className="who-we-serve-item-name">{name}</div>
      <div className="who-we-serve-item-text">{text}</div>
    </div>
  );
}

export default WhoWeServeItem;
