import './NewsBlogContent.css';

import NewsBlogContentAuthor from './NewsBlogContentAuthor';

function NewsBlogContent({ text, authorInfo }) {
  return (
    <div className="news-blog-content-block">
      <div className="container">
        <div className="news-blog-content-block-wrapper">
          <div
            className="news-blog-content-block-main"
            // eslint-disable-next-line react/no-danger
            dangerouslySetInnerHTML={{ __html: text }}
          />
        </div>
        <div className="news-blog-content-block-author-block">
          <NewsBlogContentAuthor info={authorInfo} />
        </div>
      </div>
    </div>
  );
}

export default NewsBlogContent;
