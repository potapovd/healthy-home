import './NewsBlogContent.css';
import { nanoid } from 'nanoid';
import ShareIcon from '../../../assets/imgs/socials/share.svg';

function NewsBlogContentAuthor({ info }) {
  return (
    <div className="news-blog-content-block-author-out">
      <div className="news-blog-content-block-author-main">
        <div className="news-blog-content-block-author-photo-block">
          <img src={info.photo} className="news-blog-content-block-author-photo" alt="" />
        </div>
        <div className="news-blog-content-block-author-info">
          <div className="news-blog-content-block-author-text">{info.text}</div>
          <div className="news-blog-content-block-author-name">{info.name}</div>
        </div>
      </div>
      <div className="news-blog-content-block-author-links">
        <div className="news-blog-content-block-author-links-tags">
          {info.tags.map(tag => (
            <div key={nanoid()} className="news-blog-content-block-tag">
              {tag}
            </div>
          ))}
        </div>
        <div className="news-blog-content-block-author-links-list">
          <img src={ShareIcon} className="news-blog-content-block-icon-share" alt="" />
          {info.links.map(link => (
            <div key={nanoid()} className="news-blog-content-block-icon-out">
              <img src={link.linkIcon} className="news-blog-content-block-icon" alt="" />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default NewsBlogContentAuthor;
