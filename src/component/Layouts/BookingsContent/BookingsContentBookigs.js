import React from 'react';

import './BookingsContent.css';
import { nanoid } from 'nanoid';
import BookingsInputSerch from '../../Form/BookingsInputSerch/BookingsInputSerch';
import BookingsContentBookingItem from './BookingsContentBookigItem';

function BookingsContentBookings({ list }) {
  // const [bookingsList, setBookingsList] = useState(list);
  // const [activeList, setactiveList] = useState('all');

  // TODO -FILTRATION

  //   const filterList = (listName) => {
  //     setactiveList(listName);
  //   };

  return (
    <div className="booking-content-form-wrapper">
      <div className="booking-content-form-name">My Bookings</div>
      <div className="booking-content-section">
        <div className="booking-content-tabs-search">
          <div className="booking-content-tabs">
            <div
              id="booking-list-all"
              //   onKeyDown={() => {
              //     filterList('all');
              //   }}
              //   onClick={() => {
              //     filterList('all');
              //   }}
              className="booking-content-tab booking-content-tab-active"
            >
              All Bookings
            </div>
            <div
              id="booking-list-all"
              //   onClick={() => {
              //     filterList('all');
              //   }}
              className="booking-content-tab"
            >
              Upcoming
            </div>
            <div
              id="booking-list-all"
              //   onClick={() => {
              //     filterList('all');
              //   }}
              className="booking-content-tab"
            >
              Completed
            </div>
          </div>
          <div className="booking-content-search">
            <BookingsInputSerch id="booking-search" name="booking-search" placeholder="Search bookings" />
          </div>
        </div>

        <div className="booking-content-bookings-list">
          {/* {bookingsList.map((item) => ( */}
          {list.map(item => (
            <BookingsContentBookingItem key={nanoid()} item={item} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default BookingsContentBookings;
