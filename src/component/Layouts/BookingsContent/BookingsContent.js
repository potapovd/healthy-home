import './BookingsContent.css';
// TODO - MOVE ProfileContentAside
import ProfileContentAside from '../ProfileContent/ProfileContentAside';
import BookingsContentBookings from './BookingsContentBookigs';

function BookingsContent({ data }) {
  return (
    <div className="profile-content-block">
      <div className="container">
        <div className="profile-content-inner">
          <ProfileContentAside />
          <div className="profile-content-form">
            <BookingsContentBookings list={data} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default BookingsContent;
