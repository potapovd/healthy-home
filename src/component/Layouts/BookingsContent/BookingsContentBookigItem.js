import './BookingsContent.css';

import React, { useState } from 'react';

import { nanoid } from 'nanoid';

import CounterWithButtons from '../../Elements/CounterWithButtons/CounterWithButtons';
import IconDataSection from '../../Elements/IconDataSection/IconDataSection';
import TypeOfProperty from '../../Elements/TypeOfProperty/TypeOfProperty';
import ButtonAction from '../../Form/Button/ButtonAction';
import Textarea from '../../Form/Textarea/Textarea';

function UpdateForm() {
  // eslint-disable-next-line no-alert
  alert('Update!');
}

function BookingsContentBookingItem({ item }) {
  const [isActive, setIsActive] = useState(false);

  return (
    <div className="booking-content-bookings-list-item">
      <div
        role="button"
        tabIndex={0}
        onKeyDown={() => setIsActive(!isActive)}
        onClick={() => setIsActive(!isActive)}
        className="booking-content-bookings-header"
      >
        <div className="booking-content-bookings-header-left">
          <div className="booking-content-bookings-header-name">{item.name}</div>
          <div className="booking-content-bookings-header-categories">
            {item.path.map(pathItem => (
              <div key={nanoid()} className="booking-content-bookings-header-categor-wrapper">
                <div className="booking-content-bookings-header-category">{pathItem}</div>
                <svg
                  className="booking-content-bookings-header-category-icon"
                  width="6"
                  height="12"
                  viewBox="0 0 6 12"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    d="M4.09277 5.99813L0.214998 2.12718C0.0774089 1.98969 7.122e-05 1.80318 4.91653e-08 1.60867C-7.11217e-05 1.41416 0.0771295 1.22759 0.214618 1.09C0.352107 0.952408 0.538622 0.875071 0.733132 0.875C0.927642 0.874929 1.11421 0.952129 1.2518 1.08962L5.64494 5.47972C5.77766 5.61295 5.85393 5.79219 5.85789 5.9802C5.86185 6.16821 5.79319 6.35051 5.6662 6.4892L1.2518 10.9097C1.18388 10.9778 1.10319 11.0319 1.01436 11.0688C0.925536 11.1058 0.830301 11.1249 0.734095 11.125C0.637889 11.1251 0.542597 11.1063 0.453661 11.0696C0.364724 11.033 0.283884 10.9791 0.215757 10.9112C0.14763 10.8433 0.0935486 10.7626 0.056602 10.6737C0.0196554 10.5849 0.000566631 10.4897 0.000425671 10.3935C0.000284711 10.2973 0.0190943 10.202 0.0557804 10.113C0.0924665 10.0241 0.146311 9.94327 0.214239 9.87514L4.09277 5.99813Z"
                    fill="#515668"
                  />
                </svg>
              </div>
            ))}
          </div>
          <div className="booking-content-bookings-header-details">
            <div className="booking-content-bookings-header-detail">
              <div className="booking-content-bookings-header-detail-name">Date: </div>
              <div className="booking-content-bookings-header-detail-value">{item.date}</div>
            </div>
            <div className="booking-content-bookings-header-detail">
              <div className="booking-content-bookings-header-detail-name">Price: </div>
              <div className="booking-content-bookings-header-detail-value booking-content-bookings-header-detail-price">
                {item.price}
              </div>
            </div>
            <div className="booking-content-bookings-header-detail">
              <div className="booking-content-bookings-header-detail-name">Status: </div>
              <div className="booking-content-bookings-header-detail-value">{item.status}</div>
            </div>
          </div>
        </div>
        <div className="booking-content-bookings-header-right">
          <div
            className={`booking-content-bookings-header-btn booking-btn-${
              item.status === 'Upcoming' ? 'modify' : 'rebook'
            }`}
          >
            {item.status === 'Upcoming' ? 'Modify' : 'Rebook'}
          </div>
        </div>
      </div>

      <div
        className={`booking-content-bookings-info
            ${isActive ? 'booking-content-bookings-info-active' : ''}
         `}
      >
        <div className="booking-content-bookings-info-line">
          <div className="booking-content-bookings-info-name">Type of Property</div>
          <TypeOfProperty list={item.typesList} active={item.typeActive} />
        </div>

        {item.subTypes.map(subType => (
          <div key={nanoid()} className="booking-content-bookings-info-line">
            <div className="booking-content-bookings-info-name">{subType.name}</div>
            <CounterWithButtons value={subType.value} />
          </div>
        ))}

        <div className="booking-content-bookings-info-line">
          <div className="booking-content-bookings-info-name">Cleaning instructions</div>
          <div className="booking-content-bookings-info-instructions">
            <Textarea id="instructions" name="instructions" placeholder="" label="" value={item.instructions} />
          </div>
        </div>

        <div className="booking-content-bookings-info-line">
          <div className="booking-content-form-w45">
            <div className="booking-content-bookings-info-name">
              Manage Addresses
              <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                  <path
                    d="M13.4583 8.53292C13.0666 8.53292 12.75 8.8503 12.75 9.24121V14.9079C12.75 15.2982 12.4326 15.6162 12.0417 15.6162H2.125C1.73396 15.6162 1.41671 15.2982 1.41671 14.9079V4.99121C1.41671 4.60094 1.73396 4.28292 2.125 4.28292H7.79171C8.1834 4.28292 8.5 3.96555 8.5 3.57463C8.5 3.18359 8.1834 2.86621 7.79171 2.86621H2.125C0.953423 2.86621 0 3.81963 0 4.99121V14.9079C0 16.0795 0.953423 17.0329 2.125 17.0329H12.0417C13.2133 17.0329 14.1667 16.0795 14.1667 14.9079V9.24121C14.1667 8.84952 13.85 8.53292 13.4583 8.53292Z"
                    fill="#F4A418"
                  />
                  <path
                    d="M6.64081 7.88753C6.59127 7.93707 6.55793 8.00011 6.5438 8.06807L6.04303 10.5728C6.01968 10.6889 6.05652 10.8086 6.14004 10.8929C6.20736 10.9602 6.29802 10.9963 6.39088 10.9963C6.41345 10.9963 6.43692 10.9942 6.46027 10.9893L8.96425 10.4885C9.03364 10.4743 9.09668 10.4411 9.14557 10.3914L14.7499 4.78706L12.2459 2.2832L6.64081 7.88753Z"
                    fill="#F4A418"
                  />
                  <path
                    d="M16.4813 0.551191C15.7908 -0.13946 14.6673 -0.13946 13.9773 0.551191L12.9971 1.53146L15.5011 4.03544L16.4813 3.05504C16.8157 2.72146 16.9999 2.27659 16.9999 1.80344C16.9999 1.3303 16.8157 0.885428 16.4813 0.551191Z"
                    fill="#F4A418"
                  />
                </g>
                <defs>
                  <clipPath id="clip0">
                    <rect width="17" height="17" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </div>

            <IconDataSection icon={item.addressIcon} name={item.addressName} text={item.address} />
          </div>
        </div>

        <div className="booking-content-bookings-info-line">
          <div className="booking-content-form-w45">
            <div className="booking-content-bookings-info-name">
              Booking Date
              <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0)">
                  <path
                    d="M13.4583 8.53292C13.0666 8.53292 12.75 8.8503 12.75 9.24121V14.9079C12.75 15.2982 12.4326 15.6162 12.0417 15.6162H2.125C1.73396 15.6162 1.41671 15.2982 1.41671 14.9079V4.99121C1.41671 4.60094 1.73396 4.28292 2.125 4.28292H7.79171C8.1834 4.28292 8.5 3.96555 8.5 3.57463C8.5 3.18359 8.1834 2.86621 7.79171 2.86621H2.125C0.953423 2.86621 0 3.81963 0 4.99121V14.9079C0 16.0795 0.953423 17.0329 2.125 17.0329H12.0417C13.2133 17.0329 14.1667 16.0795 14.1667 14.9079V9.24121C14.1667 8.84952 13.85 8.53292 13.4583 8.53292Z"
                    fill="#F4A418"
                  />
                  <path
                    d="M6.64081 7.88753C6.59127 7.93707 6.55793 8.00011 6.5438 8.06807L6.04303 10.5728C6.01968 10.6889 6.05652 10.8086 6.14004 10.8929C6.20736 10.9602 6.29802 10.9963 6.39088 10.9963C6.41345 10.9963 6.43692 10.9942 6.46027 10.9893L8.96425 10.4885C9.03364 10.4743 9.09668 10.4411 9.14557 10.3914L14.7499 4.78706L12.2459 2.2832L6.64081 7.88753Z"
                    fill="#F4A418"
                  />
                  <path
                    d="M16.4813 0.551191C15.7908 -0.13946 14.6673 -0.13946 13.9773 0.551191L12.9971 1.53146L15.5011 4.03544L16.4813 3.05504C16.8157 2.72146 16.9999 2.27659 16.9999 1.80344C16.9999 1.3303 16.8157 0.885428 16.4813 0.551191Z"
                    fill="#F4A418"
                  />
                </g>
                <defs>
                  <clipPath id="clip0">
                    <rect width="17" height="17" fill="white" />
                  </clipPath>
                </defs>
              </svg>
            </div>
            <IconDataSection icon={item.dateIcon} name="Date & Time" text={item.dateTime} />
          </div>
        </div>

        <div className="booking-content-bookings-info-line booking-content-bookings-info-line-btns">
          {/* <button type="button" class="booking-content-bookings-update-btn">
            Update
          </button> */}
          <ButtonAction label="Update" isUppercase size="m" onClickAction={UpdateForm} />

          <button type="button" className="booking-content-bookings-cancel-btn">
            Cancel This Booking
          </button>
        </div>
      </div>
    </div>
  );
}

export default BookingsContentBookingItem;
