import './BlogSliderLast.css';

function BlogSliderLastItem({ image, name, date, link }) {
  return (
    <div className="blog-slider-item">
      {/* TODO - LINK */}
      <img src={image} className="blog-slider-item-img" alt="" />
      <div className="blog-slider-item-block-info">
        <div className="blog-slider-item-date">{date}</div>
        <div className="blog-slider-item-name">
          {name} {link}
        </div>
      </div>
    </div>
  );
}

export default BlogSliderLastItem;
