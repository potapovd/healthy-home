import './BlogSliderLast.css';
import { nanoid } from 'nanoid';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

import BlogSliderLastItem from './BlogSliderLastItem';

function BlogSliderLast({ blockName, data }) {
  const responsive = {
    768: {
      items: 2,
    },
    991: {
      items: 3,
    },
    1280: {
      items: 4,
    },
    1500: {
      items: 5,
    },
  };
  const handleOnDragStart = e => e.preventDefault();
  return (
    <div className="blog-slider-list-block">
      <div className="container">
        <div className="blog-slider-block-name">{blockName}</div>
        <div className="blog-slider">
          <AliceCarousel
            fadeOutAnimation
            mouseTracking
            disableDotsControls
            infinite
            responsive={responsive}
            // autoPlayDirection="rtl"
          >
            {data.map(dataItem => (
              <BlogSliderLastItem
                key={nanoid()}
                image={dataItem.image}
                name={dataItem.name}
                date={dataItem.date}
                link={dataItem.link}
              />
            ))}
          </AliceCarousel>
        </div>
      </div>
    </div>
  );
}

export default BlogSliderLast;
