import './WhyUsNumbers.css';

import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';

function WhyUsNumberItem({ data }) {
  return (
    <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
      {({ isVisible }) => (
        <div className="why-us-numbers-block-number-item">
          {data.isRating ? (
            <div className="why-us-numbers-block-number">
              <CountUp end={data.number} decimals={2} duration={3} /> / {data.ratingMax}
            </div>
          ) : (
            <div className="why-us-numbers-block-number">
              <CountUp end={data.number} duration={4} decimal="," separator=" " />
              {data.postfix && data.postfix}
            </div>
          )}
          <div className="why-us-numbers-block-number-text">{data.descr}</div>
        </div>
      )}
    </VisibilitySensor>
  );
}

export default WhyUsNumberItem;
