import './WhyUsNumbers.css';

import WhyUsNumberItem from './WhyUsNumberItem';

const NumbersData = [
  { number: 4.5, descr: 'Google Rating ', isRating: true, ratingMax: 5 },
  { number: 35, descr: 'Happy Clients', postfix: 'k+' },
  { number: 5, descr: 'Country Service' },
  { number: 50, descr: 'Completed Service', postfix: 'k+' },
];

function WhyUsNumbers() {
  return (
    <div className="why-us-numbers-block">
      <div className="container">
        <div className="why-us-numbers-block-wrapper">
          <div className="why-us-numbers-block-name">What’s the numbers</div>
          <div className="why-us-numbers-block-numbers">
            {NumbersData.map(item => (
              <WhyUsNumberItem data={item} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default WhyUsNumbers;
