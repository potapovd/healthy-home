// import React from 'react';
import React, { useState } from 'react';

import './BookingContent.css';

import BookingImage1 from '../../../assets/imgs/booking/ac-1.png';
import BookingImage2 from '../../../assets/imgs/booking/ac-2.png';
import BookingImage3 from '../../../assets/imgs/booking/ac-3.png';
import HomeIcon from '../../../assets/imgs/addess-home.png';
import WorkIcon from '../../../assets/imgs/addess-work.png';
import MasterCardImage from '../../../assets/imgs/payment/master-card.png';
import PopupImgMask from '../../../assets/imgs/popup-mask.png';
import FullTextImg from '../../../assets/imgs/booking/ac-full.jpg';

import BookingProgress from './BookingProgress';
import BookingStep1 from './BookingStep1';
import BookingStep2 from './BookingStep2';
import BookingStep3 from './BookingStep3';
import BookingStep4 from './BookingStep4';

const bookingDataObj = [
  {
    name: 'Combo - Deep Vacuuming, Shampooing and Sanitization',

    text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
    price: 'AED 176.19',
    vat: 'AED 8.81',
    totalPrice: 'AED 185.00',
    img: BookingImage1,
    nameShort: 'AC Cleaning',
    namePath: ['AC Duct', 'AC Duct cleaning'],
    textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
    fullTextDescription: `<p>Duration: 10-15 minutes</p>
    <p>What is included: </p>
    <p>DHA Licensed Technicians: </p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Comfortable & Hassle-Free: </p>
    <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
    <p>Safety Guaranteed:</p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
    fullTextImg: FullTextImg,
  },

  {
    name: 'Full AC System Cleaning (Duct + Coil)',
    text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
    price: 'AED 176.19',
    vat: 'AED 8.81',
    totalPrice: 'AED 185.00',
    img: BookingImage2,
    nameShort: 'AC Cleaning2',
    namePath: ['AC Duct', 'AC Duct cleaning'],
    textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
    fullTextDescription: `<p>Duration: 10-15 minutes</p>
    <p>What is included: </p>
    <p>DHA Licensed Technicians: </p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Comfortable & Hassle-Free: </p>
    <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
    <p>Safety Guaranteed:</p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
    fullTextImg: FullTextImg,
  },
  {
    name: 'AC- Split Unit',
    text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
    price: 'AED 176.19',
    vat: 'AED 8.81',
    totalPrice: 'AED 185.00',
    img: BookingImage3,
    nameShort: 'AC Cleaning3',
    namePath: ['AC Duct', 'AC Duct cleaning'],
    textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
    fullTextDescription: `<p>Duration: 10-15 minutes</p>
    <p>What is included: </p>
    <p>DHA Licensed Technicians: </p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Comfortable & Hassle-Free: </p>
    <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
    <p>Safety Guaranteed:</p>
    <ul><li>Our DHA licensed technician will come equipped with all the necessary
    Protective Personal Equipment </li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
    <p>Same-Day Availability:</p>
    <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
    fullTextImg: FullTextImg,
  },
];

const cardsData = [{ image: MasterCardImage, number: '0213' }];

const addressesData = [
  { icon: HomeIcon, name: 'HOME', text: '4261 Kembery Drive, Chicago, LSA' },
  { icon: WorkIcon, name: 'OFFICE', text: '4262 Kembery Drive, Chicago, LSA' },
];

function BookingContentAC() {
  const [page, setPage] = useState(1);
  const [serviceIndex, setServiceIndex] = useState(false);

  const [acNumber, setAcNumber] = useState(0);
  const [bedroomsNumber, setBedroomsNumber] = useState(0);
  const [typeofProperty, setTypeOfProperty] = useState('Appartments');

  const [instructions, setInstructions] = useState('');
  const [date, setDate] = useState('...');
  const [time, setTime] = useState('...');
  const [address, setAddress] = useState('...');

  const dataAsideObj = {
    details: [
      { name: 'Type of Property', value: typeofProperty, id: 'typeProp' },
      { name: 'Number of Bedrooms', value: bedroomsNumber, id: 'bedrooms' },
      { name: 'Number of AC Units', value: acNumber, id: 'acs' },
    ],
    date,
    time,
    address,
  };
  const details = [
    { name: 'Type of Property', value: typeofProperty, type: 'typeOfProperty', id: 'typeProp' },
    { name: 'Number of Bedrooms', value: bedroomsNumber, type: 'counter', id: 'bedrooms' },
    { name: 'Number of AC Units', value: acNumber, type: 'counter', id: 'acs' },
  ];

  const bookingData = { ...bookingDataObj[serviceIndex], details };

  //   steps
  const goNextPage = () => {
    if (page === 4) return;
    setPage(page + 1);
  };
  const goPrevPage = () => {
    if (page < 0) return;
    setPage(page - 1);
  };

  const handleServiceIndex = index => {
    setServiceIndex(index);
  };

  const handleSetDetail = (value, id) => {
    // console.log(value, id);
    if (id === 'acs') {
      setAcNumber(value);
    } else if (id === 'bedrooms') {
      setBedroomsNumber(value);
    } else if (id === 'typeProp') {
      setTypeOfProperty(value);
    } else if (id === 'instructions') {
      setInstructions(value);
    } else if (id === 'date') {
      setDate(value);
    } else if (id === 'time') {
      setTime(value);
    } else if (id === 'address') {
      setAddress(value);
    }
    return null;
  };

  return (
    <>
      <BookingProgress active={page} />

      {page === 1 && (
        <BookingStep1 data={bookingDataObj} goNextPageAction={goNextPage} changeServiceIndex={handleServiceIndex} />
      )}

      {page === 2 && (
        <BookingStep2
          data={bookingData}
          goNextPageAction={goNextPage}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          updateDetail={handleSetDetail}
          instructionsTextarea={instructions}
        />
      )}

      {page === 3 && (
        <BookingStep3
          data={bookingData}
          goNextPageAction={goNextPage}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          updateDetail={handleSetDetail}
          timeValue={time}
          dataValue={date}
          addressValue={address}
          addressesData={addressesData}
        />
      )}
      {page === 4 && (
        <BookingStep4
          data={bookingData}
          serviceIndex={serviceIndex}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          popupImg={PopupImgMask}
          cardsData={cardsData}
        />
      )}
    </>
  );
}

export default BookingContentAC;
