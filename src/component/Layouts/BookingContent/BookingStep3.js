import { nanoid } from 'nanoid';
import React, { useState } from 'react';
import IconDataSection from '../../Elements/IconDataSection/IconDataSection';
import BookingAddress from '../../Elements/PopupElement/BookingAddress';
import TypeOfProperty from '../../Elements/TypeOfProperty/TypeOfProperty';
import BookingBtnNextBack from './BookingBtnNextBack';
import BookingStepsAside from './BookingStepsAside';

const getDayName = (dateStr, locale, version) => {
  const date = new Date(dateStr);
  return date.toLocaleDateString(locale, { weekday: version });
};

function BookingStep3({
  data,
  goNextPageAction,
  goPrevPageAction,
  dataAside,
  updateDetail,
  timeValue,
  dataValue,
  addressValue,
  addressesData,
}) {
  const [showAddress, setShowAddress] = useState(false);

  // next 7 days
  const dates = [...Array(7)].map((_, i) => {
    const d = new Date();
    d.setDate(d.getDate() + i);
    d.setHours(0, 0, 0, 0);
    return d;
  });

  return (
    <div className="service-booking-block">
      <div className="container">
        <div className="service-booking-block-content">
          <div className="service-booking-block-main">
            <div className="service-booking-block-main-form-line service-booking-block-main-form-line-icon">
              <div className="service-booking-block-main-form-line-name">
                When would you like your service?
                <div>
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M4.99994 0C4.60544 0 4.28564 0.319796 4.28564 0.714299V2.14286H5.71419V0.714299C5.71419 0.319796 5.3944 0 4.99994 0Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M14.9999 0C14.6054 0 14.2856 0.319796 14.2856 0.714299V2.14286H15.7142V0.714299C15.7142 0.319796 15.3944 0 14.9999 0Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M17.8571 2.14284H15.7143V4.99999C15.7143 5.3945 15.3945 5.71429 15 5.71429C14.6055 5.71429 14.2857 5.3945 14.2857 4.99999V2.14284H5.71427V4.99999C5.71427 5.3945 5.39447 5.71429 4.99997 5.71429C4.60547 5.71429 4.28567 5.3945 4.28567 4.99999V2.14284H2.14286C0.959388 2.14284 0 3.10223 0 4.28569V17.8571C0 19.0406 0.959388 20 2.14286 20H17.8571C19.0406 20 20 19.0406 20 17.8571V4.28569C20 3.10223 19.0406 2.14284 17.8571 2.14284ZM18.5714 17.8571C18.5714 18.2516 18.2516 18.5714 17.8571 18.5714H2.14286C1.74835 18.5714 1.42856 18.2516 1.42856 17.8571V8.5714H18.5714V17.8571Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M5.71421 9.99998H4.28565C3.89115 9.99998 3.57135 10.3198 3.57135 10.7143C3.57135 11.1088 3.89115 11.4286 4.28565 11.4286H5.71421C6.10871 11.4286 6.4285 11.1088 6.4285 10.7143C6.4285 10.3198 6.10871 9.99998 5.71421 9.99998Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M10.7143 9.99998H9.28577C8.89127 9.99998 8.57147 10.3198 8.57147 10.7143C8.57147 11.1088 8.89127 11.4286 9.28577 11.4286H10.7143C11.1088 11.4286 11.4286 11.1088 11.4286 10.7143C11.4286 10.3198 11.1088 9.99998 10.7143 9.99998Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M15.7143 9.99998H14.2857C13.8912 9.99998 13.5714 10.3198 13.5714 10.7143C13.5714 11.1088 13.8912 11.4286 14.2857 11.4286H15.7143C16.1088 11.4286 16.4286 11.1088 16.4286 10.7143C16.4286 10.3198 16.1087 9.99998 15.7143 9.99998Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M5.71421 12.8571H4.28565C3.89115 12.8571 3.57135 13.1769 3.57135 13.5714C3.57135 13.9659 3.89115 14.2857 4.28565 14.2857H5.71421C6.10871 14.2857 6.4285 13.9659 6.4285 13.5714C6.4285 13.1769 6.10871 12.8571 5.71421 12.8571Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M10.7143 12.8571H9.28577C8.89127 12.8571 8.57147 13.1769 8.57147 13.5714C8.57147 13.9659 8.89127 14.2857 9.28577 14.2857H10.7143C11.1088 14.2857 11.4286 13.9659 11.4286 13.5714C11.4286 13.1769 11.1088 12.8571 10.7143 12.8571Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M15.7143 12.8571H14.2857C13.8912 12.8571 13.5714 13.1769 13.5714 13.5714C13.5714 13.9659 13.8912 14.2857 14.2857 14.2857H15.7143C16.1088 14.2857 16.4286 13.9659 16.4286 13.5714C16.4285 13.1769 16.1087 12.8571 15.7143 12.8571Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M5.71421 15.7142H4.28565C3.89115 15.7142 3.57135 16.034 3.57135 16.4285C3.57135 16.823 3.89115 17.1428 4.28565 17.1428H5.71421C6.10871 17.1428 6.4285 16.823 6.4285 16.4285C6.4285 16.034 6.10871 15.7142 5.71421 15.7142Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M10.7143 15.7142H9.28577C8.89127 15.7142 8.57147 16.034 8.57147 16.4285C8.57147 16.823 8.89127 17.1428 9.28577 17.1428H10.7143C11.1088 17.1428 11.4286 16.823 11.4286 16.4285C11.4286 16.034 11.1088 15.7142 10.7143 15.7142Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M15.7143 15.7142H14.2857C13.8912 15.7142 13.5714 16.034 13.5714 16.4285C13.5714 16.823 13.8912 17.1428 14.2857 17.1428H15.7143C16.1088 17.1428 16.4286 16.823 16.4286 16.4285C16.4286 16.034 16.1087 15.7142 15.7143 15.7142Z"
                      fill="#4E868D"
                    />
                  </svg>
                </div>
              </div>

              <div className="service-booking-block-main-form-calendar">
                {dates.map(item =>
                  dataValue === item.getTime() ? (
                    <button
                      key={nanoid()}
                      type="button"
                      onClick={() => updateDetail(item.getTime(), 'date')}
                      className="service-booking-block-main-form-calendar-item service-booking-block-main-form-calendar-item-active"
                    >
                      <div className="service-booking-block-main-form-calendar-item-day">
                        {getDayName(item, 'en-UK', 'short')}
                      </div>
                      <div className="service-booking-block-main-form-calendar-item-numb">{item.getDate()}</div>
                    </button>
                  ) : (
                    <button
                      key={nanoid()}
                      type="button"
                      onClick={() => updateDetail(item.getTime(), 'date')}
                      className="service-booking-block-main-form-calendar-item"
                    >
                      <div className="service-booking-block-main-form-calendar-item-day">
                        {getDayName(item, 'en-UK', 'short')}
                      </div>
                      <div className="service-booking-block-main-form-calendar-item-numb"> {item.getDate()}</div>
                    </button>
                  )
                )}
              </div>
            </div>
            <div className="service-booking-block-main-form-line service-booking-block-main-form-line-icon">
              <div className="service-booking-block-main-form-line-name">
                What time would you like us to start?
                <div>
                  <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M10 0C4.48598 0 0 4.48598 0 10C0 15.514 4.48598 20 10 20C15.514 20 20 15.514 20 10C20 4.48598 15.514 0 10 0ZM14.5352 11.3803H9.80281C9.79328 11.3803 9.78406 11.3791 9.77465 11.3789C9.76523 11.3792 9.75602 11.3803 9.74648 11.3803C9.27977 11.3803 8.90141 11.0019 8.90141 10.5352V3.66199C8.90141 3.19527 9.27977 2.81691 9.74648 2.81691C10.2132 2.81691 10.5916 3.19527 10.5916 3.66199V9.69016H14.5352C15.002 9.69016 15.3803 10.0685 15.3803 10.5352C15.3803 11.002 15.0019 11.3803 14.5352 11.3803Z"
                      fill="#4E868D"
                    />
                  </svg>
                </div>
              </div>
              <div className="service-booking-block-main-form-time">
                <TypeOfProperty
                  list={[
                    '08:00 - 09:00',
                    '09:00 - 10:00',
                    '10:00 - 11:00 ',
                    '11:00 - 12:00 ',
                    '12:00 - 13:00 ',
                    '14:00 - 15:00 ',
                    '16:00 - 17:00 ',
                    '17:00 - 18:00 ',
                    '18:00 - 19:00 ',
                  ]}
                  active={timeValue}
                  updateAction={updateDetail}
                  id="time"
                />
              </div>
            </div>

            <div className="service-booking-block-main-form-line">
              <div className="service-booking-block-main-name service-booking-block-main-name-link">
                Your Address
                <button
                  type="button"
                  onClick={() => setShowAddress(!showAddress)}
                  className="service-booking-block-main-name-link-link"
                >
                  Choose /Add New Address
                </button>
              </div>
              <div className="service-booking-block-main-form-addresses">
                {addressesData.map(item => (
                  <div className="service-booking-block-main-form-line-50">
                    {addressValue === item.text ? (
                      <IconDataSection
                        key={nanoid()}
                        icon={item.icon}
                        name={item.name}
                        id="address"
                        text={item.text}
                        updateAction={updateDetail}
                        isSelected
                      />
                    ) : (
                      <IconDataSection
                        key={nanoid()}
                        icon={item.icon}
                        name={item.name}
                        id="address"
                        text={item.text}
                        updateAction={updateDetail}
                      />
                    )}
                  </div>
                ))}
              </div>
            </div>

            <BookingBtnNextBack goPrevPageAction={goPrevPageAction} goNextPageAction={goNextPageAction} />
          </div>
          <BookingStepsAside dataAside={dataAside} data={data} />
        </div>
      </div>
      <BookingAddress open={showAddress} data={data} onCloseAction={() => setShowAddress(false)} />
    </div>
  );
}

export default BookingStep3;
