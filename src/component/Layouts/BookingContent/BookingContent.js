import { Redirect } from 'react-router-dom';
import BookingContentAC from './BookingContentAC';
import BookingContentCarpet from './BookingContentCarpet';
import BookingContentCurtains from './BookingContentCurtains';
import BookingContentMattress from './BookingContentMattress';
import BookingContentSofa from './BookingContentSofa';
import BookingContentWaterTank from './BookingContentWaterTank';
import BookingContentDisinfectionPestControl from './BookingContentDisinfectionPestControl';

function BookingContent({ service }) {
  if (service === 'ac') {
    return <BookingContentAC />;
  }
  if (service === 'carpet') {
    return <BookingContentCarpet />;
  }
  if (service === 'curtains') {
    return <BookingContentCurtains />;
  }
  if (service === 'mattress') {
    return <BookingContentMattress />;
  }
  if (service === 'sofa') {
    return <BookingContentSofa />;
  }
  if (service === 'water-tank') {
    return <BookingContentWaterTank />;
  }
  if (service === 'disinfection-pest-control') {
    return <BookingContentDisinfectionPestControl />;
  }
  return <Redirect to="/404" />;
}

export default BookingContent;
