import React, { useState } from 'react';
import { nanoid } from 'nanoid';
import BookingBtnNextBack from './BookingBtnNextBack';
import BookingStepsAside from './BookingStepsAside';
import PaymentsCard from '../../Elements/PaymentsCard/PaymentsCard';
import InputRadioCircle from '../../Form/InputRadioCircle/InputRadioCircle';
import InputText from '../../Form/InputText/InputText';
import BookingHealth from '../../Elements/PopupElement/BookingHealth';
import BookingCard from '../../Elements/PopupElement/BookingCard';
import BookingSuccess from '../../Elements/PopupElement/BookingSuccess';

const getDayName = (dateStr, locale, version) => {
  const date = new Date(dateStr);
  return date.toLocaleDateString(locale, { weekday: version });
};

function BookingStep4({ data, goPrevPageAction, dataAside, popupImg, cardsData }) {
  const [showSelfHealthDeclaration, setShowSelfHealthDeclaration] = useState(true);
  const [showCongratulation, setShowCongratulation] = useState(false);
  const [newCard, setNewCard] = useState(false);

  const handleFinish = () => setShowCongratulation(!showCongratulation);

  return (
    <div className="service-booking-block">
      <div className="container">
        <div className="service-booking-block-content">
          <div className="service-booking-block-main">
            <div className="service-booking-block-main-form-line">
              <div className="service-booking-block-main-form-credit">
                <div className="service-booking-block-main-form-credit-name">
                  <svg
                    className="service-booking-block-main-form-credit-name-icon"
                    width="18"
                    height="18"
                    viewBox="0 0 18 18"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M4.20649 3.6499L11.3822 1.69123L11.0022 0.923121C10.7536 0.423851 10.1472 0.217675 9.64793 0.466299L3.22412 3.6499H4.20649Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M13.6097 1.74776C13.5208 1.74776 13.4319 1.75989 13.3429 1.78414L11.6572 2.245L6.50879 3.64983H12.3525H14.8994L14.584 2.49363C14.4627 2.04287 14.0544 1.74776 13.6097 1.74776Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M15.9524 4.35724H15.722H15.4086H15.0953H12.7041H3.91735H2.76519H1.79495H1.61505H1.01269C0.693318 4.35724 0.40831 4.5048 0.222347 4.73725C0.137451 4.84438 0.0727681 4.96768 0.0363841 5.10311C0.0141494 5.18801 0 5.27695 0 5.36791V5.48919V6.64135V16.6288C0 17.1867 0.452779 17.6394 1.01067 17.6394H15.9504C16.5083 17.6394 16.961 17.1867 16.961 16.6288V13.809H10.9637C10.0157 13.809 9.24559 13.0389 9.24559 12.0909V11.1651V10.8518V10.5385V9.84314C9.24559 9.37824 9.43154 8.9558 9.73273 8.64653C9.99955 8.37161 10.3573 8.18565 10.7576 8.13916C10.8243 8.13109 10.893 8.12703 10.9617 8.12703H16.1202H16.4335H16.7468H16.961V5.36791C16.9631 4.81002 16.5103 4.35724 15.9524 4.35724Z"
                      fill="#4E868D"
                    />
                    <path
                      d="M17.6706 9.09327C17.5695 9.00028 17.4503 8.92954 17.3169 8.88305C17.2138 8.8487 17.1046 8.82849 16.9894 8.82849H16.9631H16.9429H16.6296H15.4997H10.9638C10.4059 8.82849 9.95312 9.28125 9.95312 9.83916V10.3425V10.6558V10.9691V12.0889C9.95312 12.6468 10.4059 13.0996 10.9638 13.0996H16.9631H16.9894C17.1046 13.0996 17.2138 13.0793 17.3169 13.045C17.4503 13.0005 17.5695 12.9277 17.6706 12.8348C17.8727 12.6508 18.0001 12.384 18.0001 12.0889V9.83916C18.0001 9.54402 17.8727 9.27719 17.6706 9.09327ZM13.0741 11.1651C13.0741 11.4441 12.8477 11.6705 12.5687 11.6705H12.2332C11.9543 11.6705 11.7279 11.4441 11.7279 11.1651V10.8296C11.7279 10.6679 11.8027 10.5244 11.9219 10.4334C12.0088 10.3667 12.116 10.3243 12.2332 10.3243H12.3181H12.5687C12.8477 10.3243 13.0741 10.5506 13.0741 10.8296V11.1651Z"
                      fill="#4E868D"
                    />
                  </svg>
                  Use Credit first
                </div>
                <div className="service-booking-block-main-form-credit-amout">AED 300</div>
              </div>
            </div>

            <div className="service-booking-block-main-form-line">
              <div className="service-booking-block-main-form-line-name">Payment Method?</div>
              <InputRadioCircle
                name=""
                inputName="payby"
                inputs={[
                  { id: 'card', label: 'Pay by debit/ credit card' },
                  { id: 'cash', label: 'Pay with cash' },
                ]}
              />
              <div className="service-booking-block-main-form-addresses">
                <div className="service-booking-block-main-form-line-50">
                  {cardsData.map(card => (
                    <div key={nanoid()}>
                      <PaymentsCard cardData={card} />
                      <button
                        type="button"
                        onClick={() => setNewCard(!newCard)}
                        className="service-booking-block-main-name-link-link"
                      >
                        Add New Card
                      </button>
                    </div>
                  ))}
                </div>
                <div className="service-booking-block-main-form-line-50"> </div>
              </div>
            </div>

            <div className="service-booking-block-main-form-line">
              <div className="service-booking-block-main-form-line-name">Add Voucher</div>
              <div className="service-booking-block-main-form-line-30">
                <InputText type="text" id="voucher" name="voucher" placeholder="Add Voucher Code" label="" />
              </div>
            </div>

            <BookingBtnNextBack goPrevPageAction={goPrevPageAction} goNextPageAction={handleFinish} />
          </div>

          <BookingStepsAside dataAside={dataAside} data={data} />
        </div>
      </div>
      <BookingHealth
        open={showSelfHealthDeclaration}
        onCloseAction={() => setShowSelfHealthDeclaration(false)}
        PopupImgMask={popupImg}
      />
      <BookingCard open={newCard} onClose={() => setNewCard(false)} />
      <BookingSuccess open={showCongratulation} onClose={() => setShowCongratulation(false)} />
    </div>
  );
}

export default BookingStep4;
