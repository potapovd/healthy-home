import { nanoid } from 'nanoid';
import BookingAsideItem from './BookingAsideItem';

const getDayName = (dateStr, locale, version) => {
  const date = new Date(dateStr);
  return date.toLocaleDateString(locale, { weekday: version });
};

function BookingStepsAside({ data, dataAside }) {
  const date = new Date(dataAside.date);
  return (
    <div className="service-booking-block-aside">
      <div className="service-booking-block-aside-name">Summary</div>
      <div className="service-booking-block-aside-section">
        <div className="service-booking-block-aside-section-name">
          <div className="service-booking-block-aside-section-name-name">Price Details</div>
          <div className="service-booking-block-aside-section-name-icon">
            <svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.8906 6.37254L0.256909 1.74701C0.0924987 1.58272 8.51034e-05 1.35985 5.87495e-08 1.12742C-8.49859e-05 0.894993 0.0921648 0.672052 0.256455 0.507641C0.420746 0.34323 0.643619 0.250818 0.876046 0.250732C1.10847 0.250647 1.33141 0.342897 1.49583 0.507187L6.74535 5.75308C6.90394 5.91228 6.99508 6.12647 6.99981 6.35113C7.00454 6.57578 6.9225 6.79362 6.77074 6.95935L1.49583 12.2415C1.41466 12.3229 1.31825 12.3876 1.2121 12.4317C1.10596 12.4759 0.992157 12.4987 0.877197 12.4988C0.762237 12.499 0.648369 12.4765 0.542096 12.4327C0.435822 12.3888 0.339224 12.3245 0.257816 12.2433C0.176408 12.1622 0.111785 12.0658 0.0676358 11.9596C0.0234869 11.8535 0.000677088 11.7397 0.00050865 11.6247C0.000340211 11.5097 0.0228164 11.3959 0.066654 11.2896C0.110492 11.1833 0.174832 11.0867 0.256002 11.0053L4.8906 6.37254Z"
                fill="#515668"
              />
            </svg>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">Price</div>
            <div className="service-booking-block-aside-section-list-value">{data.price}</div>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">VAT</div>
            <div className="service-booking-block-aside-section-list-value">{data.vat}</div>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">Total Price</div>
            <div className="service-booking-block-aside-section-list-value service-booking-block-aside-section-list-value-bold">
              {data.totalPrice}
            </div>
          </div>
        </div>
      </div>
      <div className="service-booking-block-aside-section">
        <div className="service-booking-block-aside-section-name">
          <div className="service-booking-block-aside-section-name-name">Service Details</div>
          <div className="service-booking-block-aside-section-name-icon">
            <svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.8906 6.37254L0.256909 1.74701C0.0924987 1.58272 8.51034e-05 1.35985 5.87495e-08 1.12742C-8.49859e-05 0.894993 0.0921648 0.672052 0.256455 0.507641C0.420746 0.34323 0.643619 0.250818 0.876046 0.250732C1.10847 0.250647 1.33141 0.342897 1.49583 0.507187L6.74535 5.75308C6.90394 5.91228 6.99508 6.12647 6.99981 6.35113C7.00454 6.57578 6.9225 6.79362 6.77074 6.95935L1.49583 12.2415C1.41466 12.3229 1.31825 12.3876 1.2121 12.4317C1.10596 12.4759 0.992157 12.4987 0.877197 12.4988C0.762237 12.499 0.648369 12.4765 0.542096 12.4327C0.435822 12.3888 0.339224 12.3245 0.257816 12.2433C0.176408 12.1622 0.111785 12.0658 0.0676358 11.9596C0.0234869 11.8535 0.000677088 11.7397 0.00050865 11.6247C0.000340211 11.5097 0.0228164 11.3959 0.066654 11.2896C0.110492 11.1833 0.174832 11.0867 0.256002 11.0053L4.8906 6.37254Z"
                fill="#515668"
              />
            </svg>
          </div>
        </div>

        {dataAside.details.map(item => (
          <BookingAsideItem key={nanoid()} item={item} />
        ))}
      </div>
      <div className="service-booking-block-aside-section">
        <div className="service-booking-block-aside-section-name">
          <div className="service-booking-block-aside-section-name-name">Date & Time</div>
          <div className="service-booking-block-aside-section-name-icon">
            <svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.8906 6.37254L0.256909 1.74701C0.0924987 1.58272 8.51034e-05 1.35985 5.87495e-08 1.12742C-8.49859e-05 0.894993 0.0921648 0.672052 0.256455 0.507641C0.420746 0.34323 0.643619 0.250818 0.876046 0.250732C1.10847 0.250647 1.33141 0.342897 1.49583 0.507187L6.74535 5.75308C6.90394 5.91228 6.99508 6.12647 6.99981 6.35113C7.00454 6.57578 6.9225 6.79362 6.77074 6.95935L1.49583 12.2415C1.41466 12.3229 1.31825 12.3876 1.2121 12.4317C1.10596 12.4759 0.992157 12.4987 0.877197 12.4988C0.762237 12.499 0.648369 12.4765 0.542096 12.4327C0.435822 12.3888 0.339224 12.3245 0.257816 12.2433C0.176408 12.1622 0.111785 12.0658 0.0676358 11.9596C0.0234869 11.8535 0.000677088 11.7397 0.00050865 11.6247C0.000340211 11.5097 0.0228164 11.3959 0.066654 11.2896C0.110492 11.1833 0.174832 11.0867 0.256002 11.0053L4.8906 6.37254Z"
                fill="#515668"
              />
            </svg>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">Date</div>
            <div className="service-booking-block-aside-section-list-value">
              {dataAside.date !== '...'
                ? `  ${getDayName(date, 'en-UK', 'long')},
                      ${date.getDate()}/
                      ${date.getMonth() + 1}/
                      ${date.getFullYear()}`
                : '...'}
            </div>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">Time</div>
            <div className="service-booking-block-aside-section-list-value">{dataAside.time}</div>
          </div>
        </div>
      </div>
      <div className="service-booking-block-aside-section">
        <div className="service-booking-block-aside-section-name">
          <div className="service-booking-block-aside-section-name-name">Address</div>
          <div className="service-booking-block-aside-section-name-icon">
            <svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M4.8906 6.37254L0.256909 1.74701C0.0924987 1.58272 8.51034e-05 1.35985 5.87495e-08 1.12742C-8.49859e-05 0.894993 0.0921648 0.672052 0.256455 0.507641C0.420746 0.34323 0.643619 0.250818 0.876046 0.250732C1.10847 0.250647 1.33141 0.342897 1.49583 0.507187L6.74535 5.75308C6.90394 5.91228 6.99508 6.12647 6.99981 6.35113C7.00454 6.57578 6.9225 6.79362 6.77074 6.95935L1.49583 12.2415C1.41466 12.3229 1.31825 12.3876 1.2121 12.4317C1.10596 12.4759 0.992157 12.4987 0.877197 12.4988C0.762237 12.499 0.648369 12.4765 0.542096 12.4327C0.435822 12.3888 0.339224 12.3245 0.257816 12.2433C0.176408 12.1622 0.111785 12.0658 0.0676358 11.9596C0.0234869 11.8535 0.000677088 11.7397 0.00050865 11.6247C0.000340211 11.5097 0.0228164 11.3959 0.066654 11.2896C0.110492 11.1833 0.174832 11.0867 0.256002 11.0053L4.8906 6.37254Z"
                fill="#515668"
              />
            </svg>
          </div>
        </div>
        <div className="service-booking-block-aside-section-list">
          <div className="service-booking-block-aside-section-list-item">
            <div className="service-booking-block-aside-section-list-name">{dataAside.address}</div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default BookingStepsAside;
