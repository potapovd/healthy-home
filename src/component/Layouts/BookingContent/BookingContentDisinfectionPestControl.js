// import React from 'react';
import React, { useState } from 'react';

import './BookingContent.css';

import { nanoid } from 'nanoid';
import BookingImage1Car from '../../../assets/imgs/booking/disinfection-car-1.png';
import BookingImage2Car from '../../../assets/imgs/booking/disinfection-car-2.png';
import BookingImage3Car from '../../../assets/imgs/booking/disinfection-car-3.png';
import BookingImage1Home from '../../../assets/imgs/booking/disinfection-home-1.png';
import BookingImage1Office from '../../../assets/imgs/booking/disinfection-office-1.png';
import BookingImage2Office from '../../../assets/imgs/booking/disinfection-office-2.png';
import BookingImage3Office from '../../../assets/imgs/booking/disinfection-office-3.png';
import HomeIcon from '../../../assets/imgs/addess-home.png';
import WorkIcon from '../../../assets/imgs/addess-work.png';
import MasterCardImage from '../../../assets/imgs/payment/master-card.png';
import PopupImgMask from '../../../assets/imgs/popup-mask.png';
import FullTextImg from '../../../assets/imgs/booking/ac-full.jpg';

import BookingProgress from './BookingProgress';
import BookingStep1 from './BookingStep1';
import BookingStep2 from './BookingStep2';
import BookingStep3 from './BookingStep3';
import BookingStep4 from './BookingStep4';

const bookingDataObj = [
  {
    nameCat: 'Car Disinfection',
    list: [
      {
        detailsId: 'car',
        name: 'Combo1 - Deep Vacuuming, Shampooing and Sanitization',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage1Car,
        nameShort: 'AC Cleaning',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
      <p>What is included: </p>
      <p>DHA Licensed Technicians: </p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Comfortable & Hassle-Free: </p>
      <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
      <p>Safety Guaranteed:</p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
      {
        detailsId: 'car',
        name: 'Full2 AC System Cleaning (Duct + Coil)',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage2Car,
        nameShort: 'AC Cleaning2',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
      <p>What is included: </p>
      <p>DHA Licensed Technicians: </p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Comfortable & Hassle-Free: </p>
      <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
      <p>Safety Guaranteed:</p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
      {
        detailsId: 'car',
        name: 'AC3- Split Unit',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage3Car,
        nameShort: 'AC Cleaning3',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
      <p>What is included: </p>
      <p>DHA Licensed Technicians: </p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Comfortable & Hassle-Free: </p>
      <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
      <p>Safety Guaranteed:</p>
      <ul><li>Our DHA licensed technician will come equipped with all the necessary
      Protective Personal Equipment </li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
      <p>Same-Day Availability:</p>
      <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
    ],
  },
  {
    nameCat: 'Home Disinfection',
    list: [
      {
        detailsId: 'home',
        name: 'Combo4 - Deep Vacuuming, Shampooing and Sanitization',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage1Home,
        nameShort: 'AC Cleaning',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
        <p>What is included: </p>
        <p>DHA Licensed Technicians: </p>
        <ul><li>Our DHA licensed technician will come equipped with all the necessary
        Protective Personal Equipment </li></ul>
        <p>Comfortable & Hassle-Free: </p>
        <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
        <p>Safety Guaranteed:</p>
        <ul><li>Our DHA licensed technician will come equipped with all the necessary
        Protective Personal Equipment </li></ul>
        <p>Same-Day Availability:</p>
        <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
        <p>Same-Day Availability:</p>
        <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
    ],
  },
  {
    nameCat: 'Office Disinfection',
    list: [
      {
        detailsId: 'office',
        name: 'Combo5 - Deep Vacuuming, Shampooing and Sanitization',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage1Office,
        nameShort: 'AC Cleaning',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
       <p>What is included: </p>
       <p>DHA Licensed Technicians: </p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Comfortable & Hassle-Free: </p>
       <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
       <p>Safety Guaranteed:</p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
      {
        detailsId: 'office',
        name: 'Full6 AC System Cleaning (Duct + Coil)',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage2Office,
        nameShort: 'AC Cleaning2',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
       <p>What is included: </p>
       <p>DHA Licensed Technicians: </p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Comfortable & Hassle-Free: </p>
       <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
       <p>Safety Guaranteed:</p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
      {
        detailsId: 'office',
        name: 'AC7- Split Unit',
        text: 'Remove all dust mites, stains and spots from your carpet in 45 minutes',
        price: 'AED 176.19',
        vat: 'AED 8.81',
        totalPrice: 'AED 185.00',
        img: BookingImage3Office,
        nameShort: 'AC Cleaning3',
        namePath: ['AC Duct', 'AC Duct cleaning'],
        textDescription: `<p>Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food trucks. In publishing and graphic design, Lorem ipsum is a placeholder text our in commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available. In publishing and graphic design, Lorem ipsum</p>`,
        fullTextDescription: `<p>Duration: 10-15 minutes</p>
       <p>What is included: </p>
       <p>DHA Licensed Technicians: </p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Comfortable & Hassle-Free: </p>
       <ul><li>Our 3 mm flocked nasal swab will sufficiently collect enough genetic material to test your sample, for a more comfortable and pleasant experience</li></ul>
       <p>Safety Guaranteed:</p>
       <ul><li>Our DHA licensed technician will come equipped with all the necessary
       Protective Personal Equipment </li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>
       <p>Same-Day Availability:</p>
       <ul><li>Our team is available for home testing in only 60 minutes</li></ul>`,
        fullTextImg: FullTextImg,
      },
    ],
  },
];

const cardsData = [{ image: MasterCardImage, number: '0213' }];

const addressesData = [
  { icon: HomeIcon, name: 'HOME', text: '4261 Kembery Drive, Chicago, LSA' },
  { icon: WorkIcon, name: 'OFFICE', text: '4262 Kembery Drive, Chicago, LSA' },
];

function BookingContentDisinfectionPestControl() {
  const [page, setPage] = useState(1);
  const [serviceIndex, setServiceIndex] = useState(0);

  const [sedanNumber, setSedanNumber] = useState(0);
  const [suvNumber, setSuvNumber] = useState(0);
  const [bedroomsNumber, setBedroomsNumber] = useState(0);
  const [typeofProperty, setTypeOfProperty] = useState('Appartments');

  const [instructions, setInstructions] = useState('');
  const [date, setDate] = useState('...');
  const [time, setTime] = useState('...');
  const [address, setAddress] = useState('...');

  // detailsId: car|home|office
  const dataAsideObjFull = [
    {
      detailsId: 'car',
      detailsList: [
        { name: 'Sedan1', value: sedanNumber, id: 'sedanNumber' },
        { name: 'SUV', value: suvNumber, id: 'suvNumber' },
      ],
    },
    {
      detailsId: 'home',
      detailsList: [
        { name: 'Type of Property', value: typeofProperty, id: 'typeProp' },
        { name: 'Number of Bedroom', value: bedroomsNumber, id: 'bedroomsNumber' },
      ],
    },
    {
      detailsId: 'office',
      detailsList: [
        { name: 'Type of Property', value: typeofProperty, id: 'typeProp' },
        { name: 'Number of Bedroom', value: bedroomsNumber, id: 'bedroomsNumber' },
      ],
    },
  ];
  let detailsListAside;
  dataAsideObjFull.forEach(element => {
    if (element.detailsId === serviceIndex) {
      detailsListAside = element.detailsList;
    }
  });
  const dataAsideObj = {
    // details: dataAsideObjFull[serviceIndex].detailsList,
    details: detailsListAside,
    date,
    time,
    address,
  };
  const detailsFull = [
    {
      detailsId: 'car',
      detailsList: [
        { name: 'Sedan1', value: sedanNumber, type: 'counter', id: 'sedanNumber' },
        { name: 'SUV', value: suvNumber, type: 'counter', id: 'suvNumber' },
      ],
    },
    {
      detailsId: 'home',
      detailsList: [
        { name: 'Type of Property', value: typeofProperty, type: 'typeOfProperty', id: 'typeProp' },
        { name: 'Number of Bedroom', value: bedroomsNumber, type: 'counter', id: 'bedroomsNumber' },
      ],
    },
    {
      detailsId: 'office',
      detailsList: [
        { name: 'Type of Property', value: typeofProperty, type: 'typeOfProperty', id: 'typeProp' },
        { name: 'Number of Bedroom', value: bedroomsNumber, type: 'counter', id: 'bedroomsNumber' },
      ],
    },
  ];

  let details;
  detailsFull.forEach(element => {
    if (element.detailsId === serviceIndex) {
      details = element.detailsList;
    }
  });

  // const details = detailsFull.detailsId === 'car';
  // const bookingData = { ...bookingDataObj[0].list[serviceIndex], details };
  const bookingData = { ...bookingDataObj[0].list[0], details };

  //   steps
  const goNextPage = () => {
    if (page === 4) return;
    setPage(page + 1);
  };
  const goPrevPage = () => {
    if (page < 0) return;
    setPage(page - 1);
  };

  const handleServiceIndex = index => {
    setServiceIndex(index);
  };

  const handleSetDetail = (value, id) => {
    // console.log(value, id);
    if (id === 'sedanNumber') {
      setSedanNumber(value);
    } else if (id === 'suvNumber') {
      setSuvNumber(value);
    } else if (id === 'typeProp') {
      setTypeOfProperty(value);
    } else if (id === 'bedroomsNumber') {
      setBedroomsNumber(value);
    } else if (id === 'instructions') {
      setInstructions(value);
    } else if (id === 'date') {
      setDate(value);
    } else if (id === 'time') {
      setTime(value);
    } else if (id === 'address') {
      setAddress(value);
    }
    return null;
  };

  return (
    <>
      <BookingProgress active={page} />

      {page === 1 && (
        <BookingStep1
          data={bookingDataObj}
          goNextPageAction={goNextPage}
          changeServiceIndex={handleServiceIndex}
          categoriesMultiLevel
        />
      )}

      {page === 2 && (
        <BookingStep2
          data={bookingData}
          goNextPageAction={goNextPage}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          updateDetail={handleSetDetail}
          instructionsTextarea={instructions}
        />
      )}

      {page === 3 && (
        <BookingStep3
          data={bookingData}
          goNextPageAction={goNextPage}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          updateDetail={handleSetDetail}
          timeValue={time}
          dataValue={date}
          addressValue={address}
          addressesData={addressesData}
        />
      )}
      {page === 4 && (
        <BookingStep4
          data={bookingData}
          serviceIndex={serviceIndex}
          goPrevPageAction={goPrevPage}
          dataAside={dataAsideObj}
          popupImg={PopupImgMask}
          cardsData={cardsData}
        />
      )}
    </>
  );
}

export default BookingContentDisinfectionPestControl;
