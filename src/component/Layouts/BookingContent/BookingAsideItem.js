function BookingAsideItem({ item }) {
  return (
    <div className="service-booking-block-aside-section-list">
      <div className="service-booking-block-aside-section-list-item">
        <div className="service-booking-block-aside-section-list-name">{item.name}</div>
        <div className="service-booking-block-aside-section-list-value">{item.value}</div>
      </div>
    </div>
  );
}

export default BookingAsideItem;
