import { nanoid } from 'nanoid';

const steps = [
  { step: 1, name: 'Service' },
  { step: 2, name: 'Details' },
  { step: 3, name: 'Date & Time' },
  { step: 4, name: 'Payment' },
];

function BookingProgress({ active }) {
  return (
    <div className="service-bookinge-block">
      <div className="container">
        <div className="service-booking-progress">
          {steps.map(step => (
            <div
              key={nanoid()}
              className={`service-booking-progress-item ${
                step.step <= active ? 'service-booking-progress-item-active' : ''
              } `}
            >
              <div className="service-booking-progress-line" />
              <div className="service-booking-progress-item-data">
                <div className="service-booking-progress-item-number">{step.step}</div>
                <div className="service-booking-progress-item-name">{step.name}</div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default BookingProgress;
