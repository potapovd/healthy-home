import { nanoid } from 'nanoid';
import React, { useState } from 'react';
import CounterWithButtons from '../../Elements/CounterWithButtons/CounterWithButtons';
import BookingFullDetails from '../../Elements/PopupElement/BookingFullDetails';
import TypeOfProperty from '../../Elements/TypeOfProperty/TypeOfProperty';
import Textarea from '../../Form/Textarea/Textarea';
import BookingBtnNextBack from './BookingBtnNextBack';
import BookingStepsAside from './BookingStepsAside';

function DetailItem({ item, updateDetail }) {
  if (item.type === 'counter') {
    return (
      <div className="service-booking-block-main-form-line-counter" key={nanoid()}>
        <div>
          <div className="service-booking-block-main-form-counter-name">{item.name}</div>
          {item.description ? (
            <div className="service-booking-block-main-form-counter-description">{item.description}</div>
          ) : (
            ' '
          )}
        </div>
        <div className="service-booking-block-main-form-counter">
          <CounterWithButtons value={item.value} updateAction={updateDetail} id={item.id} />
        </div>
      </div>
    );
  }
  if (item.type === 'typeOfProperty') {
    return (
      <div className="service-booking-block-main-form-line">
        <div className="service-booking-block-main-form-line-name">Type of Property</div>
        <TypeOfProperty
          list={['Appartments', 'Villa', 'Office']}
          active={item.value}
          updateAction={updateDetail}
          id={item.id}
        />
      </div>
    );
  }
}

function BookingStep2({
  data,
  goNextPageAction,
  goPrevPageAction,
  dataAside,
  updateDetail,
  instructionsTextarea,
  dataMulitiLevel = false,
}) {
  const [showDetails, setShowDetails] = useState(false);

  function handleSetShowDetails() {
    setShowDetails(!showDetails);
  }

  function DetailItemRender({ dataRender, multilevel = false }) {
    if (multilevel) {
      return dataRender.details.map(itemOut => (
        <>
          <div className="service-booking-block-main-form-counter-name-top">{itemOut.name}</div>
          <div className="service-booking-block-main-form-counter-out">
            {itemOut.details.map(item => (
              <DetailItem key={nanoid()} item={item} updateDetail={updateDetail} />
            ))}
          </div>
        </>
      ));
    }

    return dataRender.details.map(item => <DetailItem key={nanoid()} item={item} updateDetail={updateDetail} />);
  }

  return (
    <div className="service-booking-block">
      <div className="container">
        <div className="service-booking-block-content">
          <div className="service-booking-block-main">
            <div className="service-booking-block-main-name">{data.nameShort}</div>
            <div className="service-booking-block-bread-crumbs">
              {data.namePath.map(item => (
                <div key={nanoid()}>
                  <div className="service-booking-block-bread-crumbs-text">{item}</div>
                  <svg
                    className="service-booking-block-bread-crumbs-icon"
                    width="5"
                    height="9"
                    viewBox="0 0 5 9"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M0 7.48252L2.98252 4.5L0 1.53301L0.916505 0.616501L4.8 4.5L0.916505 8.38349L0 7.48252Z"
                      fill="#353535"
                    />
                  </svg>
                </div>
              ))}
            </div>
            <div className="service-booking-block-main-text">
              <div
                // eslint-disable-next-line react/no-danger
                dangerouslySetInnerHTML={{ __html: data.textDescription }}
              />
              <button
                type="button"
                onClick={handleSetShowDetails}
                onKeyDown={handleSetShowDetails}
                className="service-booking-block-main-text-more"
              >
                Read More
              </button>
            </div>

            <DetailItemRender dataRender={data} multilevel={dataMulitiLevel} />

            <div className="service-booking-block-main-form-line">
              <div className="service-booking-block-main-form-line-name">
                Do you have any specific cleaning instructions?
              </div>
              <Textarea
                id="instructions"
                name="instructions"
                value={instructionsTextarea}
                updateAction={updateDetail}
                placeholder=""
              />
            </div>

            <BookingBtnNextBack goPrevPageAction={goPrevPageAction} goNextPageAction={goNextPageAction} />
          </div>
          <BookingStepsAside dataAside={dataAside} data={data} />
        </div>
      </div>
      <BookingFullDetails open={showDetails} data={data} onCloseAction={() => setShowDetails(false)} />
    </div>
  );
}

export default BookingStep2;
