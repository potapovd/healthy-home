function BookingItem({ item, index, goNextPageAction, changeServiceIndex }) {
  return (
    <div className="service-booking-item">
      <img src={item.img} className="service-booking-item-img" alt="" />
      <div className="service-booking-item-description">
        <div className="service-booking-item-name">{item.name}</div>
        <div className="service-booking-item-text">{item.text}</div>
        <div className="service-booking-item-price-next">
          <div className="service-booking-item-price">
            Price starting from: <span>{item.price}</span>
          </div>
          {item.detailsId ? (
            <div
              role="button"
              tabIndex={0}
              onClick={() => {
                goNextPageAction();
                changeServiceIndex(item.detailsId);
              }}
              onKeyDown={goNextPageAction}
              className="service-booking-item-btn"
            >
              Continue
            </div>
          ) : (
            <div
              role="button"
              tabIndex={0}
              onClick={() => {
                goNextPageAction();
                changeServiceIndex(index);
              }}
              onKeyDown={goNextPageAction}
              className="service-booking-item-btn"
            >
              Continue
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
export default BookingItem;
