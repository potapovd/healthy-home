function BookingBtnNextBack({ goPrevPageAction, goNextPageAction }) {
  return (
    <div className="service-booking-block-main-form-line">
      <div
        className="service-booking-item-btn-back"
        role="button"
        tabIndex={0}
        onClick={goPrevPageAction}
        onKeyDown={goPrevPageAction}
      >
        Back
      </div>
      <div
        className="service-booking-item-btn"
        role="button"
        tabIndex={0}
        onClick={goNextPageAction}
        onKeyDown={goNextPageAction}
      >
        Continue
      </div>
    </div>
  );
}

export default BookingBtnNextBack;
