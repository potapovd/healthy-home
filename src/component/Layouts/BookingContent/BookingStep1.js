import { nanoid } from 'nanoid';
import BookingItem from './BookingItem';

function BookingStep1({ data, goNextPageAction, changeServiceIndex, categoriesMultiLevel = false }) {
  function ServiceBookingListRender({ dataList, multiLevel = false }) {
    if (multiLevel) {
      return dataList.map(itemOut => (
        <div key={nanoid()}>
          <div className="service-booking-block-main-form-counter-name-top">{itemOut.nameCat}</div>
          <div className="service-booking-list">
            {itemOut.list.map((item, index) => (
              <BookingItem
                key={nanoid()}
                item={item}
                index={index}
                goNextPageAction={goNextPageAction}
                changeServiceIndex={changeServiceIndex}
              />
            ))}
          </div>
        </div>
      ));
    }
    return (
      <div className="service-booking-list">
        {dataList.map((item, index) => (
          <BookingItem
            key={nanoid()}
            item={item}
            index={index}
            goNextPageAction={goNextPageAction}
            changeServiceIndex={changeServiceIndex}
          />
        ))}
      </div>
    );
  }

  return (
    <div className="service-booking-block">
      <div className="container">
        <ServiceBookingListRender dataList={data} multiLevel={categoriesMultiLevel} />
      </div>
    </div>
  );
}

export default BookingStep1;
