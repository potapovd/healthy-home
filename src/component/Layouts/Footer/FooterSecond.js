import './Footer.css';

import { Link } from 'react-router-dom';

import AmericanExpress from '../../../assets/imgs/payment/american-express.png';
import MasterCard from '../../../assets/imgs/payment/master-card.png';
import Maestro from '../../../assets/imgs/payment/mestro.png';
import Payoneer from '../../../assets/imgs/payment/payoneer.png';
import Visa from '../../../assets/imgs/payment/visa.png';

function FooterSecond() {
  return (
    <div className="footer-block-second">
      <div className="container-footer">
        <div className="footer-block-second-out">
          <div className="footer-block-second-text">
            <div className="footer-block-second-text-copy">
              Copyright © 2021 The Healthy Home Global Ltd. All Rights Reserved.
              <Link className="footer-block-second-text-link" to="/">
                T&Cs
              </Link>
              <Link className="footer-block-second-text-link" to="/">
                Privacy Policy
              </Link>
              <Link className="footer-block-second-text-link" to="/">
                Refund Policy
              </Link>
            </div>
          </div>
          <div className="footer-block-second-payment">
            <div className="footer-block-second-payment-icon-out">
              <img className="footer-block-second-payment-icon" src={Visa} alt="" />
            </div>
            <div className="footer-block-second-payment-icon-out">
              <img className="footer-block-second-payment-icon" src={MasterCard} alt="" />
            </div>
            <div className="footer-block-second-payment-icon-out">
              <img className="footer-block-second-payment-icon" src={AmericanExpress} alt="" />
            </div>
            <div className="footer-block-second-payment-icon-out">
              <img className="footer-block-second-payment-icon" src={Payoneer} alt="" />
            </div>
            <div className="footer-block-second-payment-icon-out">
              <img className="footer-block-second-payment-icon" src={Maestro} alt="" />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default FooterSecond;
