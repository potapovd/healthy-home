import './ServiceTop.css';

import VideoPlayer from '../../Elements/VideoPlayer/VideoPlayer';
import ButtonLink from '../../Form/Button/ButtonLink';

function ServiceTop({ videoData }) {
  return (
    <div className="service-top-block">
      <div className="container service-top-inner">
        <div className="service-top-left">
          <h1 className="service-top-header">Carpets Cleaning-Home Services</h1>
          <div className="service-top-text">
            Asertively maintain click and mortar quality vectors through next-generation technologies. Asertively
            maintain click and mortar quality vectors through next-generation. Maintaining a house spic and span is a
            cumbersome task. Though regular dusting and moping make your house appear
          </div>
          <ButtonLink url="booking/ac" label="Book Now" isBlock fontSize={18} size="l" />
        </div>
        <div className="service-top-right">
          <VideoPlayer data={videoData} />
        </div>
      </div>
    </div>
  );
}

export default ServiceTop;
