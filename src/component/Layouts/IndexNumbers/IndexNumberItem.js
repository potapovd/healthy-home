import './IndexNumbers.css';
import CountUp from 'react-countup';
import VisibilitySensor from 'react-visibility-sensor';

function IndexNumberItem({ data }) {
  return (
    <div className="numbers-block-item">
      <VisibilitySensor partialVisibility offset={{ bottom: 200 }}>
        {({ isVisible }) => (
          <>
            {data.isRating ? (
              <div className="numbers-block-item-number">
                <CountUp end={data.number} decimals={2} duration={1} />/{data.ratingMax}
              </div>
            ) : (
              <div className="numbers-block-item-number">
                <CountUp end={data.number} duration={4} decimal="," prefix="+" separator=" " />
              </div>
            )}
            <div className="numbers-block-item-descr">{data.descr}</div>
          </>
        )}
      </VisibilitySensor>
    </div>
  );
}

export default IndexNumberItem;
