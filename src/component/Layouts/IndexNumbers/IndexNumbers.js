import './IndexNumbers.css';

import { nanoid } from 'nanoid';
import IndexNumberItem from './IndexNumberItem';

function IndexNumbers({ data, length }) {
  return (
    <div className="numbers-block">
      <div className="container numbers-block-inner">
        <div className="numbers-block-0">
          <div className="numbers-block-1">
            Your environment can have a positive <br /> impact on your health and wellbeing.
          </div>
          <div className="numbers-block-2">
            We are passionate about the importance of a better and healthier <br /> lifestyle for you and your family.
          </div>
        </div>
        <div className="numbers-block-3">
          <div className="numbers-block-column">
            {data.map(item => (
              <IndexNumberItem key={nanoid()} data={item} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default IndexNumbers;
