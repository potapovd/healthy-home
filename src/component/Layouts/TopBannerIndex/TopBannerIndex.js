import './TopBannerIndex.css';

import Select from 'react-select';

import Section1Img from '../../../assets/imgs/index-section-1.png';

const options = [
  { value: 'city1', label: 'City 1' },
  { value: 'city2', label: 'City 2' },
  { value: 'city3', label: 'City 3' },
];

const customStyles = {
  option: provided => ({
    ...provided,
    border: 'none',

    ':active': {
      backgroundColor: '#4e868d',
    },
  }),

  control: () => ({
    width: 200,
  }),
  dropdownIndicator: () => ({
    display: 'none',
  }),
  indicatorSeparator: () => ({
    display: 'none',
  }),
};

function TopBannerIndex() {
  return (
    <div className="section1-text-img container">
      <div className="section1-text-img-inner">
        <div className="section1-text1">Lead a healthy lifestyle</div>
        <div className="section1-text2">A world of health &amp; wellness in the palm of your hands.</div>
        <div className="section1-search-secition">
          <div className="section1-search-wrapper">
            <svg
              className="section1-search-icon"
              width="18"
              height="19"
              viewBox="0 0 18 19"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <ellipse
                cx="8.80541"
                cy="8.80541"
                rx="7.49047"
                ry="7.49046"
                stroke="#7A838F"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
              <path
                d="M14.0151 14.4043L16.9518 17.3334"
                stroke="#7A838F"
                strokeWidth="1.5"
                strokeLinecap="round"
                strokeLinejoin="round"
              />
            </svg>

            <div className="section1-search-wrapper-mobile">
              <input type="search" className="section1-search-input" placeholder="What service do you need?" />
              <div className="section1-search-select-wrapper">
                <Select
                  className="section1-search-select"
                  styles={customStyles}
                  placeholder="Choose your City"
                  options={options}
                />
              </div>
            </div>
            <button type="button" className="section1-search-button">
              <svg width="22" height="22" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M21.479 18.8169L16.4211 13.7585C17.2804 12.3904 17.7792 10.7733 17.7792 9.03786C17.7792 4.12808 13.799 0.148438 8.88942 0.148438C3.97983 0.148438 0 4.12808 0 9.03786C0 13.9478 3.97964 17.9271 8.88942 17.9271C10.778 17.9271 12.5273 17.3366 13.9668 16.3333L18.9646 21.3315C19.3118 21.6784 19.7671 21.8512 20.2218 21.8512C20.677 21.8512 21.1317 21.6784 21.4796 21.3315C22.1735 20.6367 22.1735 19.5114 21.479 18.8169ZM8.88942 15.0471C5.57098 15.0471 2.8806 12.3569 2.8806 9.03823C2.8806 5.71961 5.57098 3.02922 8.88942 3.02922C12.208 3.02922 14.8982 5.71961 14.8982 9.03823C14.8982 12.3569 12.208 15.0471 8.88942 15.0471Z"
                  fill="white"
                />
              </svg>
            </button>
          </div>
        </div>
        <div className="section1-text3">
          <div className="section1-text-btn">Ac Duct Cleaning</div>
          <div className="section1-text-btn">Pest Control</div>
          <div className="section1-text-btn">Carpet Cleaning</div>
          <div className="section1-text-btn">Disinfection &amp; Sanitization</div>
        </div>
      </div>
      <div className="section1-img-wrapper">
        <img src={Section1Img} className="section1-img" alt="" />
      </div>
    </div>
  );
}

export default TopBannerIndex;
