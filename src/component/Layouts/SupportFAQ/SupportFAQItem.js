import React, { useState } from 'react';

import './SupportFAQ.css';

function SupportFAQItem({ dataItem }) {
  const [isActive, setIsActive] = useState(false);

  return (
    <div className="support-faq-item">
      <div
        role="button"
        tabIndex={0}
        className="support-faq-item-name"
        onKeyDown={() => setIsActive(!isActive)}
        onClick={() => setIsActive(!isActive)}
      >
        {dataItem.name}
        <div className="support-faq-item-icon">{isActive ? '-' : '+'}</div>
      </div>
      {isActive && <div className="support-faq-item-text">{dataItem.text}</div>}
    </div>
  );
}

export default SupportFAQItem;
