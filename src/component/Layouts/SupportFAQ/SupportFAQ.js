import './SupportFAQ.css';
import { nanoid } from 'nanoid';
import SupportFAQItem from './SupportFAQItem';

function SupportFAQ({ data }) {
  const half = Math.ceil(data.length / 2);
  const firstHalf = data.slice(0, half);
  const secondHalf = data.slice(-half);
  return (
    <div className="support-faq-block">
      <div className="container">
        <div className="support-faq-block-name">Frequently Asked Questions</div>
        <div className="support-faq-inner">
          <div className="support-faq-inner-col">
            {firstHalf.map(dataItem => (
              <SupportFAQItem key={nanoid()} dataItem={dataItem} />
            ))}
          </div>
          <div className="support-faq-inner-col">
            {secondHalf.map(dataItem => (
              <SupportFAQItem key={nanoid()} dataItem={dataItem} />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SupportFAQ;
