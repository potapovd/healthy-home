import './Contacts.css';

import { Link } from 'react-router-dom';
import ContactsMail from '../../../assets/imgs/contacts1.png';
import ContactsPhone from '../../../assets/imgs/contacts2.png';

function Contacts() {
  return (
    <div className="contacts-block">
      <div className="container-footer contacts-block-inner">
        <div className="contacts-block-1">
          <div className="contacts-block-text1">We’re Always Here To Help</div>
          <div className="contacts-block-text2">Reach out to us through any of these support channels</div>
        </div>

        <div className="contacts-block-2-wrapper">
          <div className="contacts-block-2">
            <img src={ContactsMail} className="contacts-block-ico" alt="" />
            <div className="contacts-block-col">
              <div className="contacts-block-col-text">Email Support</div>
              <Link to="mailto:info@thehealhtyhome.me" className="contacts-block-col-link">
                info@thehealhtyhome.me
              </Link>
            </div>
          </div>
          <div className="contacts-block-2">
            <img src={ContactsPhone} className="contacts-block-ico" alt="" />
            <div className="contacts-block-col">
              <div className="contacts-block-col-text">Phone Support</div>
              <Link to="tel:80072648493" className="contacts-block-col-link">
                800 72648493
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contacts;
