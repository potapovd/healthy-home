import { nanoid } from 'nanoid';
import './Select.css';

function Select({
  id,
  name,
  label,
  options,
  labelIsUppercase,
  labelIsBlack,
  labelHasSpace,
  selectIsWhite,
  labelIsBig,
}) {
  return (
    <div className="select-field">
      <select
        className={`select-field-text
            ${selectIsWhite ? 'select-white' : ''}
         `}
        name={name}
        id={id}
      >
        {options.map(option => (
          <option key={nanoid()} value={option.value}>
            {option.name}
          </option>
        ))}
      </select>
      <label
        htmlFor={id}
        className={`select-field-label
            ${labelIsUppercase ? 'label-uppercase' : ''}
            ${labelIsBlack ? 'label-black' : ''}
            ${labelHasSpace ? 'label-space' : ''}
            ${labelIsBig ? 'label-big' : ''}
         `}
      >
        {label}
      </label>
    </div>
  );
}

export default Select;
