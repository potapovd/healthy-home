import { nanoid } from 'nanoid';
import './InputRadio.css';

function InputRadio({ name, inputName, inputs }) {
  return (
    <div className="input-radio-field">
      <div className="input-radio-field-name">{name}</div>
      <div className="input-radio-line">
        {inputs.map(input => (
          <div key={nanoid()} className="input-radio-line-item">
            <label htmlFor={input.id} className="container-radio">
              <span className="container-radio-name">{input.label}</span>
              <input type="radio" name={inputName} id={input.id} />
              <span className="container-radio-checkmark" />
            </label>
          </div>
        ))}
      </div>
    </div>
  );
}

export default InputRadio;
