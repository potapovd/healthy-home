import { nanoid } from 'nanoid';
import './InputRadioCircle.css';

function InputRadioCircle({ name, inputName, inputs }) {
  return (
    <div className="input-radio-round-field">
      {name && <div className="input-radio-round-field-name">{name}</div>}
      <div className="input-radio-round-line">
        {inputs.map(input => (
          <div key={nanoid()} className="input-radio-round-line-item">
            <label htmlFor={input.id} className="container-radio-round">
              <span className="container-radio-round-name">{input.label}</span>
              <input type="radio" name={inputName} id={input.id} onChange={() => {}} />
              <span className="container-radio-round-checkmark" />
            </label>
          </div>
        ))}
      </div>
    </div>
  );
}

export default InputRadioCircle;
