import { nanoid } from 'nanoid';
import './SelectInputText.css';

function SelectInputText({
  idInput,
  nameInput,
  placeholderInput,
  idSelect,
  nameSelect,
  label,
  options,
  labelIsUppercase,
  labelIsBlack,
  labelHasSpace,
  inputIsWhite,
}) {
  return (
    <div className="input-field select-input-fix">
      <div className="select-input-wrapper">
        <select className="select-field-text" name={nameSelect} id={idSelect}>
          {options.map(option => (
            <option key={nanoid()} value={option.value}>
              {option.name}
            </option>
          ))}
        </select>
        <input
          type="text"
          name={nameInput}
          id={idInput}
          className={`input-field-text
            ${inputIsWhite ? 'input-white' : ''}
         `}
          placeholder={placeholderInput}
        />
      </div>
      <label
        htmlFor={nameInput}
        className={`input-field-label
            ${labelIsUppercase ? 'label-uppercase' : ''}
            ${labelIsBlack ? 'label-black' : ''}
            ${labelHasSpace ? 'label-space' : ''}
         `}
      >
        {label}
      </label>
    </div>
  );
}

export default SelectInputText;
