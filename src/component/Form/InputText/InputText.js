import './InputText.css';

function InputText({
  type,
  id,
  name,
  placeholder,
  label,
  labelIsUppercase,
  labelIsBlack,
  labelHasSpace,
  inputIsWhite,
  labelIsBig,
}) {
  return (
    <div className="input-field">
      <input
        type={type}
        name={name}
        id={id}
        className={`input-field-text
            ${inputIsWhite ? 'input-white' : ''}
         `}
        placeholder={placeholder}
      />
      <label
        htmlFor={id}
        className={`input-field-label
            ${labelIsUppercase ? 'label-uppercase' : ''}
            ${labelIsBlack ? 'label-black' : ''}
            ${labelHasSpace ? 'label-space' : ''}
            ${labelIsBig ? 'label-big' : ''}
         `}
      >
        {label}
      </label>
    </div>
  );
}

export default InputText;
