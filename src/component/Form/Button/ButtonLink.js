import './Button.css';

import { Link } from 'react-router-dom';

function ButtonLink({ label, url, isBlock, isOutline, isUppercase, fontSize = 14, size = 's' }) {
  return (
    <Link
      to={url}
      style={{ fontSize }}
      className={`btn
         ${isOutline ? 'btn-outline' : ''}
         ${isUppercase ? 'btn-uppercase' : ''}
         ${isBlock ? 'btn-block' : ''}
         ${size === 's' ? 'btn-small' : ''}
         ${size === 'm' ? 'btn-medium' : ''}
         ${size === 'l' ? 'btn-large' : ''}
      `}
    >
      {label}
    </Link>
  );
}

export default ButtonLink;
