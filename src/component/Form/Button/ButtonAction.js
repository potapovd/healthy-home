import './Button.css';

function ButtonAction({ label, isBlock, isOutline, isUppercase, onClickAction, fontSize = 14, size = 's' }) {
  return (
    <button
      onClick={onClickAction}
      onKeyDown={onClickAction}
      type="button"
      style={{ fontSize }}
      className={`btn
         ${isOutline ? 'btn-outline' : ''}
         ${isUppercase ? 'btn-uppercase' : ''}
         ${isBlock ? 'btn-block' : ''}
         ${size === 's' ? 'btn-small' : ''}
         ${size === 'm' ? 'btn-medium' : ''}
         ${size === 'l' ? 'btn-large' : ''}
      `}
    >
      {label}
    </button>
  );
}

export default ButtonAction;
