import 'react-intl-tel-input/dist/main.css';
import './IntlTelInputSelect.css';

import IntlTelInput from 'react-intl-tel-input';

function IntlTelInputSelect({
  id,
  name,
  placeholder,
  label,
  labelIsUppercase,
  labelIsBlack,
  labelHasSpace,
  inputIsWhite,
}) {
  return (
    <div className="input-field intl-tel-input-fix">
      <IntlTelInput
        inputClassName={`input-field-text
            ${inputIsWhite ? 'input-white' : ''}
         `}
        separateDialCode
        preferredCountries={['ae']}
        format
        customPlaceholder="Enter you phone number"
        placeholder={placeholder}
        fieldId={id}
        fieldName={name}
        autoPlaceholder
      />

      <label
        htmlFor={id}
        className={`input-field-label
            ${labelIsUppercase ? 'label-uppercase' : ''}
            ${labelIsBlack ? 'label-black' : ''}
            ${labelHasSpace ? 'label-space' : ''}
         `}
      >
        {label}
      </label>
    </div>
  );
}

export default IntlTelInputSelect;
