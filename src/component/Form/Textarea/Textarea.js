import './Textarea.css';

function Textarea({ id, name, placeholder, label, value, updateAction = () => {} }) {
  //   const handleChange = e => {
  //     // const _value = { ...value, e.target.value };
  //     updateAction(e.target.value, id);
  //   };

  return (
    <div className="textarea-field">
      <textarea
        name={name}
        id={id}
        className="textarea-text"
        placeholder={placeholder}
        rows="7"
        onChange={e => updateAction(e.target.value, id)}
        defaultValue={value}
      />
      {label !== '' ? (
        <label htmlFor={id} className="textarea-label">
          {label}
        </label>
      ) : null}
    </div>
  );
}

export default Textarea;
