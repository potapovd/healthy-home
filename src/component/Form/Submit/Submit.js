import './Submit.css';

function Submit({ id, name, value }) {
  return (
    <div className="button-field">
      <input className="button-field-submit-btn" type="submit" id={id} name={name} value={value} />
    </div>
  );
}

export default Submit;
