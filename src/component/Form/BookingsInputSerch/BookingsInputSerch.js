import './BookingsInputSerch.css';

import SearchIcon from '../../../assets/imgs/booking-search-icon.png';

function BookingsInputSerch({ id, name, placeholder }) {
  return (
    <div className="input-field booking-search-field">
      <input
        type="search"
        name={name}
        id={id}
        className="input-field-text booking-search-input"
        placeholder={placeholder}
      />
      <img src={SearchIcon} className="booking-search-icon" alt="" />
    </div>
  );
}

export default BookingsInputSerch;
