import './NewsBlogName.css';

function NewsBlogName({ name, category, time }) {
  return (
    <div className="news-blog-name-block">
      <div className="container">
        <h1 className="news-blog-name-block-h1">{name}</h1>
        <div className="news-blog-details">
          <div className="news-blog-detail">March 20, 2019</div>
          <svg width="7" height="7" viewBox="0 0 7 7" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="3.5" cy="3.5" r="3.5" fill="#EEEEEE" />
          </svg>
          <div className="news-blog-detail">{category}</div>
          <svg width="17" height="18" viewBox="0 0 17 18" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              fillRule="evenodd"
              clipRule="evenodd"
              d="M8.49998 17.3979C13.1496 17.3979 16.9128 13.6201 16.9128 8.98509C16.9128 4.33551 13.135 0.572266 8.49998 0.572266C3.86494 0.572266 0.0871582 4.33551 0.0871582 8.98509C0.0871582 13.6201 3.85041 17.3979 8.49998 17.3979ZM8.49998 1.74919C12.4957 1.74919 15.7359 4.98936 15.7359 8.98509C15.7359 12.9808 12.4957 16.221 8.49998 16.221C4.50425 16.221 1.26408 12.9663 1.26408 8.98509C1.26408 4.98936 4.50425 1.74919 8.49998 1.74919ZM10.9119 9.97312H8.10767C7.77348 9.97312 7.51194 9.71158 7.51194 9.37739V5.32355C7.51194 4.98936 7.77348 4.72782 8.10767 4.72782C8.44186 4.72782 8.7034 4.98936 8.7034 5.32355V8.78167H10.9119C11.2461 8.78167 11.5077 9.04321 11.5077 9.37739C11.5077 9.71158 11.2461 9.97312 10.9119 9.97312Z"
              fill="white"
            />
          </svg>
          <div className="news-blog-detail">{time} min read</div>
        </div>
      </div>
    </div>
  );
}

export default NewsBlogName;
