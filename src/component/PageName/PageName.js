import './PageName.css';

function PageName({ name }) {
  return (
    <div className="pagename-block">
      <div className="container pagename-block">
        <h1 className="pagename-h1">{name}</h1>
      </div>
    </div>
  );
}

export default PageName;
