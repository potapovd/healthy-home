import './SupportName.css';

function SupportName({ name }) {
  return (
    <div className="support-name-block">
      <div className="container">
        <h1 className="support-name-block-h1">{name}</h1>
      </div>
    </div>
  );
}

export default SupportName;
