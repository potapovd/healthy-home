import { nanoid } from 'nanoid';
import './PaymentsCard.css';

function PaymentsCard({ cardData }) {
  const cardDots = [];
  for (let i = 0; i < 3; i += 1) {
    cardDots.push(
      <svg
        width="26"
        height="5"
        className="card-dots"
        viewBox="0 0 26 5"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        key={nanoid()}
      >
        <circle cx="2.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="9.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="16.5" cy="2.5" r="2.5" fill="#434355" />
        <circle cx="23.5" cy="2.5" r="2.5" fill="#434355" />
      </svg>
    );
  }
  return (
    <>
      {cardData.name && <div className="profile-content-payment-name">{cardData.name}</div>}
      <div className="profile-content-payment-wrapper">
        <div className="profile-content-card-logo">
          <img src={cardData.image} className="profile-content-card-logo-img" alt="" />
        </div>
        <div className="profile-content-card-number">
          {cardDots}
          {cardData.number}
        </div>
      </div>
    </>
  );
}

export default PaymentsCard;
