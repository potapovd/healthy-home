import React, { useState } from 'react';
import { nanoid } from 'nanoid';

import './TypeOfProperty.css';

function TypeOfProperty({ list, active, updateAction = () => {}, id }) {
  const activeIndex = list.indexOf(active);
  const [isActive, setActive] = useState(activeIndex);

  return (
    <>
      <div className="types-of-property-list">
        {list.map((type, index) => (
          <div
            role="button"
            tabIndex={0}
            key={nanoid()}
            onClick={() => {
              setActive(index);
              updateAction(list[index], id);
            }}
            onKeyDown={() => {
              setActive(index);
              updateAction(list[index], id);
            }}
            className={`types-of-property-item
               ${index === isActive ? 'types-of-property-item-active' : null}
            `}
          >
            {type}
          </div>
        ))}
      </div>
    </>
  );
}

export default TypeOfProperty;
