import React, { useState } from 'react';

import './TopNotification.css';

function TopNotification() {
  const [showBanner, setShowBanner] = useState(true);
  const onClick = () => setShowBanner(false);
  return (
    <div className="container">
      <div className={showBanner ? 'top-notification' : 'hidden-banner'}>
        Learn more about our COVID-19 safety measures
        <svg
          className="top-notification-close"
          onClick={onClick}
          width="16"
          height="16"
          viewBox="0 0 16 16"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M9.46585 8.01656L15.6959 1.78629C16.1014 1.38103 16.1014 0.725795 15.6959 0.320541C15.2907 -0.0847117 14.6354 -0.0847117 14.2302 0.320541L7.99991 6.55081L1.76983 0.320541C1.36438 -0.0847117 0.709336 -0.0847117 0.304082 0.320541C-0.101361 0.725795 -0.101361 1.38103 0.304082 1.78629L6.53417 8.01656L0.304082 14.2468C-0.101361 14.6521 -0.101361 15.3073 0.304082 15.7126C0.506045 15.9147 0.771595 16.0163 1.03696 16.0163C1.30232 16.0163 1.56768 15.9147 1.76983 15.7126L7.99991 9.4823L14.2302 15.7126C14.4323 15.9147 14.6977 16.0163 14.9631 16.0163C15.2284 16.0163 15.4938 15.9147 15.6959 15.7126C16.1014 15.3073 16.1014 14.6521 15.6959 14.2468L9.46585 8.01656Z"
            fill="black"
          />
        </svg>
      </div>
    </div>
  );
}

export default TopNotification;
