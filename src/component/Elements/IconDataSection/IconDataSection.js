import './IconDataSection.css';

function IconDataSection({ icon, id, name, text, isSelected, updateAction = () => {} }) {
  return (
    <button
      type="button"
      onClick={() => updateAction(text, id)}
      onKeyDown={() => updateAction(text, id)}
      className={`icon-data-wrapper ${isSelected ? 'icon-data-wrapper-active' : ''}`}
    >
      <div className="icon-data">
        <img src={icon} className="icon-data-img" alt="" />
      </div>
      <div className="icon-data-name">
        <div className="icon-data-name-1">{name}</div>
        <div className="icon-data-name-2">{text}</div>
      </div>
    </button>
  );
}

export default IconDataSection;
