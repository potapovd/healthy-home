import './CounterWithButtons.css';

import React, { useState } from 'react';

function CounterWithButtons({ value, updateAction, id }) {
  const [number, setNumber] = useState(value);

  const changeNumber = sign => {
    if (sign === '-') {
      if (parseInt(number - 1, 10) >= 0) {
        setNumber(parseInt(number - 1, 10));
        updateAction(parseInt(number - 1, 10), id);
      }
    } else {
      setNumber(parseInt(number + 1, 10));
      updateAction(parseInt(number + 1, 10), id);
    }
  };

  return (
    <div className="counter-with-buttons-wrapper">
      <div
        role="button"
        tabIndex={0}
        onClick={() => changeNumber('-')}
        onKeyDown={() => changeNumber('-')}
        className={`counter-with-buttons-btn-minus
                    ${number === 0 ? 'counter-with-buttons-disabled' : null}
                `}
      >
        <svg width="12" height="3" viewBox="0 0 12 3" fill="none" xmlns="http://www.w3.org/2000/svg">
          <line x1="1.25" y1="1.75" x2="10.75" y2="1.75" strokeWidth="2.5" strokeLinecap="round" />
        </svg>
      </div>
      <div className="counter-with-buttons-value">{number}</div>
      <div
        role="button"
        tabIndex={0}
        onClick={() => changeNumber('+')}
        onKeyDown={() => changeNumber('+')}
        className="counter-with-buttons-btn-plus"
      >
        <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M12.75 5.75H8.5C8.36194 5.75 8.25 5.63806 8.25 5.5V1.25C8.25 0.559692 7.69031 0 7 0C6.30969 0 5.75 0.559692 5.75 1.25V5.5C5.75 5.63806 5.63806 5.75 5.5 5.75H1.25C0.559692 5.75 0 6.30969 0 7C0 7.69031 0.559692 8.25 1.25 8.25H5.5C5.63806 8.25 5.75 8.36194 5.75 8.5V12.75C5.75 13.4403 6.30969 14 7 14C7.69031 14 8.25 13.4403 8.25 12.75V8.5C8.25 8.36194 8.36194 8.25 8.5 8.25H12.75C13.4403 8.25 14 7.69031 14 7C14 6.30969 13.4403 5.75 12.75 5.75Z" />
        </svg>
      </div>
    </div>
  );
}

export default CounterWithButtons;
