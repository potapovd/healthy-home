import { nanoid } from 'nanoid';
import { Link } from 'react-router-dom';
import './BreadCrumbs.css';

function BreadCrumbs({ path }) {
  const numb = path.length;
  return (
    <div className="breadcrumbs-block">
      <div className="container breadcrumbs-block-inner">
        <div className="breadcrumbs">
          {path.map((pathItem, index) => (
            <div className="breadcrumb-item" key={nanoid()}>
              <Link to={pathItem.link} className="breadcrumbs-text">
                {pathItem.name}
              </Link>
              {index !== numb - 1 ? (
                <div className="breadcrumbs-svg">
                  <svg width="7" height="13" viewBox="0 0 7 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                      d="M4.8906 6.12181L0.256909 1.49628C0.0924987 1.33199 8.51034e-05 1.10911 5.87495e-08 0.876687C-8.49859e-05 0.64426 0.0921648 0.421319 0.256455 0.256909C0.420746 0.0924978 0.643619 8.51034e-05 0.876046 5.87497e-08C1.10847 -8.49859e-05 1.33141 0.0921644 1.49583 0.256455L6.74535 5.50235C6.90394 5.66154 6.99508 5.87573 6.99981 6.10039C7.00454 6.32505 6.9225 6.54289 6.77074 6.70862L1.49583 11.9908C1.41466 12.0722 1.31825 12.1368 1.2121 12.181C1.10596 12.2251 0.992157 12.2479 0.877197 12.2481C0.762237 12.2483 0.648369 12.2258 0.542096 12.182C0.435822 12.1381 0.339224 12.0738 0.257816 11.9926C0.176408 11.9114 0.111785 11.815 0.0676358 11.7089C0.0234869 11.6027 0.000677088 11.4889 0.00050865 11.374C0.000340211 11.259 0.0228164 11.1451 0.066654 11.0389C0.110492 10.9326 0.174832 10.836 0.256002 10.7546L4.8906 6.12181Z"
                      fill="#DEDEDE"
                    />
                  </svg>
                </div>
              ) : null}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default BreadCrumbs;
