import '@vime/core/themes/default.css';
import './VideoPlayer.css';

import { nanoid } from 'nanoid';

import {
  Controls,
  ControlSpacer,
  DefaultUi,
  FullscreenControl,
  MuteControl,
  PlaybackControl,
  Player,
  TimeProgress,
  Video,
} from '@vime/react';

function VideoPlayer({ data }) {
  return (
    <Player playsinline autoplay muted theme="dark">
      <Video poster={data.poster}>
        {data.sources.map(item => (
          <source key={nanoid()} data-src={item.src} type={item.type} />
        ))}
      </Video>
      <DefaultUi noControls>
        <Controls pin="center">
          <ControlSpacer />
          <PlaybackControl hideTooltip style={{ '--vm-control-scale': 2.7, '--vm-control-border-radius': '50%' }} />
          <ControlSpacer />
        </Controls>
        <Controls fullWidth pin="bottomLeft">
          <TimeProgress />
          <ControlSpacer />
        </Controls>
        <Controls fullWidth pin="bottomRight">
          <ControlSpacer />
          <MuteControl />
          <FullscreenControl />
        </Controls>
      </DefaultUi>
    </Player>
  );
}

export default VideoPlayer;
