import { Link } from 'react-router-dom';
import './FixedContacts.css';

function FixedContactItem({ data }) {
  return (
    <Link to={data.link} className="fixed-conntacts-section-item">
      <div style={{ backgroundColor: data.color }} className="fixed-conntacts-background">
        <div className="fixed-conntacts-icon-out">
          <img src={data.icon} className="fixed-conntacts-icon" alt={data.name} />
        </div>
      </div>
    </Link>
  );
}

export default FixedContactItem;
