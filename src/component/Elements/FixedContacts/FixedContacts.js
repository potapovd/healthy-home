import './FixedContacts.css';

import { nanoid } from 'nanoid';
import FixedContactItem from './FixedContactItem';

import WhatsappIcon from '../../../assets/imgs/contacs-fixed-wa.png';
import MailIcon from '../../../assets/imgs/contacs-fixed-mail.png';

const contactsData = [
  { name: 'WhatsApp', icon: WhatsappIcon, link: '/', color: '#5BD68C' },
  { name: 'Mail', icon: MailIcon, link: '/support', color: '#468F91' },
];

function FixedContacts() {
  return (
    <div className="fixed-conntacts-section">
      {contactsData.map(item => (
        <FixedContactItem data={item} key={nanoid()} />
      ))}
    </div>
  );
}

export default FixedContacts;
