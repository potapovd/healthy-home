import Popup from 'reactjs-popup';
import ButtonAction from '../../Form/Button/ButtonAction';

function BookingFullDetails({ open, data, onCloseAction }) {
  return (
    <Popup open={open} onClose={() => onCloseAction} modal>
      {close => (
        <div>
          <button type="button" tabIndex="0" className="popup-content__close" onKeyDown={close} onClick={close}>
            &times;
          </button>
          <div className="popup-content__inner">
            <img src={data.fullTextImg} className="popup-content__img-ok" alt="" />
            <div
              className="popup-content__text"
              // eslint-disable-next-line react/no-danger
              dangerouslySetInnerHTML={{ __html: data.fullTextDescription }}
            />
            <div className="popup-content__content-center">
              <ButtonAction label="Continue" onClickAction={close} />
            </div>
          </div>
        </div>
      )}
    </Popup>
  );
}

export default BookingFullDetails;
