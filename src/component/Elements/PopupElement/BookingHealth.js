import React, { useState } from 'react';
import Popup from 'reactjs-popup';

function BookingHealth({ open, onCloseAction, PopupImgMask }) {
  const [confirmation, setConfirmation] = useState(false);
  const handleSetConfirmation = () => setConfirmation(!confirmation);

  return (
    <Popup open={open} onClose={onCloseAction} modal>
      {close => (
        <div>
          <button type="button" tabIndex="0" className="popup-content__close" onKeyDown={close} onClick={close}>
            &times;
          </button>
          <div className="popup-content__inner">
            <img src={PopupImgMask} className="popup-content__img-ok" alt="" />
            <div className="popup-content__header">Covid- 19 Self HealthDeclaration</div>
            <div className="popup-content__text">
              <p>
                Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local
                food trucks..
              </p>
              <ul>
                <li>Enjoy your favorite dishe and a lovely your friends and family</li>
                <li>I have not traveled interationaly in the last 14 days</li>
              </ul>
            </div>
            <div className="popup-content__confirmation">
              <label htmlFor="confirm" className="popup-content__checkbox-container">
                <div className="popup-content__checkbox-name">I Confirm that I am healty</div>
                <input id="confirm" type="checkbox" onChange={handleSetConfirmation} defaultChecked={confirmation} />
                <span className="popup-content__checkbox-checkmark" />
              </label>
            </div>

            {confirmation ? (
              <div className="popup-content__btn-ok">
                <button type="button" className="popup-content__btn-confirm" onKeyDown={close} onClick={close}>
                  Confirm
                </button>
              </div>
            ) : null}
          </div>
        </div>
      )}
    </Popup>
  );
}

export default BookingHealth;
