import Popup from 'reactjs-popup';
import ButtonAction from '../../Form/Button/ButtonAction';
import InputText from '../../Form/InputText/InputText';

function BookingCard({ open, onCloseAction }) {
  return (
    <Popup open={open} onClose={onCloseAction} modal>
      {close => (
        <div>
          <button type="button" tabIndex="0" className="popup-content__close" onKeyDown={close} onClick={close}>
            &times;
          </button>
          <div className="popup-content__inner">
            <div className="popup-content__header"> Add New Card </div>
            <div className="popup-content__line">
              <div className="popup-content__line-item-100">
                <InputText
                  type="text"
                  label="Card Holder Name"
                  id="card-name"
                  name="card-name"
                  placeholder="Card Holder Name"
                  labelIsUppercase
                />
              </div>
            </div>

            <div className="popup-content__line">
              <div className="popup-content__line-item-100">
                <InputText
                  type="text"
                  label="Card Number"
                  id="card-number"
                  name="card-number"
                  placeholder="Card Number"
                  labelIsUppercase
                />
              </div>
            </div>

            <div className="popup-content__line">
              <div className="popup-content__line-item-50">
                <InputText
                  type="text"
                  label="Expires"
                  id="card-exp"
                  name="card-exp"
                  placeholder="Card Expires"
                  labelIsUppercase
                />
              </div>
              <div className="popup-content__line-item-50">
                <InputText
                  type="text"
                  label="CVV"
                  id="card-cvv"
                  name="card-cvv"
                  placeholder="Card CVV"
                  labelIsUppercase
                />
              </div>
            </div>

            <ButtonAction
              label="Save"
              // eslint-disable-next-line no-alert
              onClickAction={() => alert('thank you!')}
            />
          </div>
        </div>
      )}
    </Popup>
  );
}

export default BookingCard;
