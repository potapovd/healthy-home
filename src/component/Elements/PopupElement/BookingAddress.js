import Popup from 'reactjs-popup';
import ButtonAction from '../../Form/Button/ButtonAction';
import InputText from '../../Form/InputText/InputText';
import Select from '../../Form/Select/Select';

function BookingAddress({ open, data, onCloseAction }) {
  return (
    <Popup open={open} onClose={onCloseAction} modal>
      {close => (
        <div>
          <button type="button" tabIndex="0" className="popup-content__close" onKeyDown={close} onClick={close}>
            &times;
          </button>
          <div className="popup-content__inner">
            <div className="popup-content__header"> Add New Address </div>
            <div className="popup-content__line">
              <div className="popup-content__line-item-50">
                <InputText type="text" label="City" id="city" name="city" placeholder="Home" labelIsUppercase />
              </div>
              <div className="popup-content__line-item-50">
                <Select
                  label="Area"
                  id="area"
                  name="area"
                  options={[
                    { name: 'Area1', value: 'area1' },
                    { name: 'Area2', value: 'area2' },
                    { name: 'Area3', value: 'area3' },
                  ]}
                  labelIsUppercase
                />
              </div>
            </div>

            <div className="popup-content__line">
              <div className="popup-content__line-item-50">
                <InputText
                  type="text"
                  label="Building/Street Name"
                  id="building"
                  name="building"
                  placeholder="home"
                  labelIsUppercase
                />
              </div>
              <div className="popup-content__line-item-50">
                <InputText
                  type="text"
                  label="Appartment/Villa"
                  id="app-villa"
                  name="app-villa"
                  placeholder="Villa"
                  labelIsUppercase
                />
              </div>
            </div>

            <div className="popup-content__line">
              <div className="popup-content__line-item-50">
                <Select
                  label="Nearest Landmark"
                  id="landmark"
                  name="landmark"
                  options={[
                    { name: 'Select1', value: 'select1' },
                    { name: 'Select2', value: 'select2' },
                    { name: 'Select3', value: 'select3' },
                  ]}
                  labelIsUppercase
                />
              </div>
            </div>

            <ButtonAction
              label="Save"
              // eslint-disable-next-line no-alert
              onClickAction={() => alert('thank you!')}
            />
          </div>
        </div>
      )}
    </Popup>
  );
}

export default BookingAddress;
