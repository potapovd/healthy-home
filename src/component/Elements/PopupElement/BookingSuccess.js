import { Link } from 'react-router-dom';
import Popup from 'reactjs-popup';

function BookingSuccess({ open, onCloseAction }) {
  return (
    <Popup open={open} onClose={onCloseAction} modal>
      {close => (
        <div>
          <button type="button" tabIndex="0" className="popup-content__close" onKeyDown={close} onClick={close}>
            &times;
          </button>
          <div className="popup-content-congratulation__inner">
            <svg
              className="popup-content_icon-success"
              width="100"
              height="100"
              viewBox="0 0 100 100"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M50.0001 0C40.1683 0 30.5574 2.91544 22.3827 8.37764C14.2079 13.8398 7.83644 21.6035 4.07401 30.6868C0.311577 39.7701 -0.672846 49.7651 1.24523 59.4079C3.1633 69.0507 7.89771 77.9082 14.8498 84.8603C21.8018 91.8123 30.6593 96.5468 40.3021 98.4648C49.9449 100.383 59.9399 99.3985 69.0232 95.636C78.1066 91.8736 85.8702 85.5022 91.3324 77.3274C96.7946 69.1526 99.71 59.5417 99.71 49.71C99.6947 36.5308 94.4526 23.8957 85.1334 14.5766C75.8143 5.25749 63.1793 0.015291 50.0001 0ZM77.7826 36.6284L46.0131 68.1488C45.1154 69.064 43.8941 69.5898 42.6124 69.6131C41.3306 69.6364 40.091 69.1552 39.1607 68.2732L22.3418 52.9494C21.393 52.0584 20.8245 50.8357 20.755 49.536C20.6854 48.2363 21.12 46.9598 21.9683 45.9727C22.4095 45.4973 22.9402 45.1137 23.5299 44.8439C24.1196 44.574 24.7568 44.4232 25.4049 44.4001C26.0531 44.3769 26.6994 44.4819 27.3069 44.709C27.9143 44.9361 28.471 45.2808 28.945 45.7235L42.2766 57.9327L70.6793 29.5269C71.6262 28.611 72.895 28.104 74.2123 28.1149C75.5296 28.1259 76.7898 28.6541 77.7213 29.5856C78.6528 30.5171 79.181 31.7773 79.1919 33.0946C79.2029 34.4118 78.6958 35.6807 77.78 36.6276L77.7826 36.6284Z"
                fill="#4DD98E"
              />
            </svg>
            <div className="popup-content__header">Your order has been Placed</div>
            <div className="popup-content__confirmation-code">Reference Code: 5D645U</div>

            <div className="popup-content__confirmation-text">
              Enjoy your favorite dishe and a lovely your friends and family and have a great time. Food from local food
              trucks..
            </div>

            <div className="popup-content__btn-ok">
              <Link to="/services" className="popup-content__btn-link">
                booked Another service
              </Link>
            </div>

            <Link to="/" className="popup-content__link-home">
              Go back to home
            </Link>
          </div>
        </div>
      )}
    </Popup>
  );
}

export default BookingSuccess;
