import './NewsCategories.css';
import TopNav from '../../component/Layouts/TopNav/TopNav';
import NewsCategoriesContent from '../../component/Layouts/NewsCategoriesContent/NewsCategoriesContent';
import BlogSliderLast from '../../component/Layouts/BlogSliderLast/BlogSliderLast';
import Feedbacks from '../../component/Layouts/Feedbacks/Feedbacks';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import BlogSlideImage1 from '../../assets/imgs/blog/blog-1.png';
import BlogSlideImage2 from '../../assets/imgs/blog/blog-2.png';
import BlogSlideImage3 from '../../assets/imgs/blog/blog-3.png';

import FeedbackImage from '../../assets/imgs/feedback/img.png';

const BlogSliderLastData = [
  {
    date: '20th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage1,
    link: '/blogpost',
  },
  {
    date: '21th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
  {
    date: '22th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage3,
    link: '/blogpost',
  },
  {
    date: '23th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
];

const FeedbackData = [
  {
    text: 'Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy through',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
  {
    text: 'Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy through',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
  {
    text: 'Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy through',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
];

function NewsCategories() {
  return (
    <>
      <section className="news-categories-banner">
        <TopNav isLight />
      </section>

      <section>
        <NewsCategoriesContent />
      </section>

      <section className="services-blog">
        <Feedbacks blockName="What our clients says about our service" data={FeedbackData} />
      </section>

      <section className="services-blog">
        <BlogSliderLast blockName="Our Latest Blog" data={BlogSliderLastData} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default NewsCategories;
