import './SignIn.css';

import SignInContent from '../../component/Layouts/Login/SignInContent';
import TopNav from '../../component/Layouts/TopNav/TopNav';

function SignIn() {
  return (
    <>
      <section className="sign-in-bg">
        <div>
          <TopNav whiteBackgroud />
        </div>
        <div className="page-name-wrapper page-name-form">
          <SignInContent />
        </div>
      </section>
    </>
  );
}

export default SignIn;
