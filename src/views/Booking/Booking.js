// import React from 'react';
import { useParams } from 'react-router-dom';
import TopNav from '../../component/Layouts/TopNav/TopNav';
import BookingContent from '../../component/Layouts/BookingContent/BookingContent';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import './Booking.css';

function Booking() {
  const { service } = useParams();

  return (
    <>
      <section className="booking-top">
        <TopNav whiteBackgroud />
        <BookingContent service={service} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Booking;
