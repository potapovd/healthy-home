import './Support.css';

import TopNav from '../../component/Layouts/TopNav/TopNav';
import SupportName from '../../component/SupportName/SupportName';
import SupportSearch from '../../component/SupportSearch/SupportSearch';
import SupportDropMessage from '../../component/Layouts/SupportDropMessage/SupportDropMessage';
import SupportFAQ from '../../component/Layouts/SupportFAQ/SupportFAQ';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import SupportImageWA from '../../assets/imgs/support-contacts-wa.png';
import SupportImagePhone from '../../assets/imgs/support-contacts-phone.png';
import SupportImageMail from '../../assets/imgs/support-contacts-mail.png';
import SupportContacts from '../../component/Layouts/SupportContacts/SupportContacts';

const SupportCotactsData = [
  {
    image: SupportImageWA,
    name: 'WhatsApp Support',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet',
    linkName: 'WhatsApp Support',
    linkURL: '#',
  },
  {
    image: SupportImagePhone,
    name: 'Give us a Call',
    text: '<p>Get in touch with our customer care:</p><p><b>+971 52 730 9507</b><p>',
    linkName: 'Give us a Call',
    linkURL: '#',
  },
  {
    image: SupportImageMail,
    name: 'Send us an Email',
    text: '<p>Get in touch with our customer care: </p><p><b>info@thehealthyhome.me</b><p>',
    linkName: 'Send us an Email',
    linkURL: '#',
  },
];

const SupportDropMessageOffices = [
  { name: 'Office Addrres (1)', address: 'Head Office Addres 2551 Alfred Drive Brooklyn, NewYork' },
  { name: 'Office Addrres (1)', address: 'Head Office Addres 2551 Alfred Drive Brooklyn, NewYork' },
  { name: 'Office Addrres (1)', address: 'Head Office Addres 2551 Alfred Drive Brooklyn, NewYork' },
];

const SupportFAQData = [
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
  {
    name: 'Our cupidatat proidenttaken possession of my entire soul Bleeding',
    text: 'Cepteur sint occaecat cupidatat proidenttaken possession of my entire soul, like these sweet mornings of spring which. Our cupidatat proidenttaken possession of my entire soul,  Bleeding convergence enterprise Enthusiastically Rapidiously whiteboard one to one paradigm.',
  },
];

function Support() {
  return (
    <>
      <section className="support-banner">
        <div className="support-nav-wrapper">
          <TopNav isLight />
        </div>
        <div className="support-name-wrapper">
          <SupportName name="Hi, how can we help?" />
          <SupportSearch />
        </div>
      </section>

      <section className="support-contacts">
        <SupportContacts data={SupportCotactsData} />
      </section>

      <section>
        <SupportFAQ data={SupportFAQData} />
      </section>

      <section className="support-drop-message">
        <SupportDropMessage data={SupportDropMessageOffices} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Support;
