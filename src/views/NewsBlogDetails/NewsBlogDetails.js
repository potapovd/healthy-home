import './NewsBlogDetails.css';

import TopNav from '../../component/Layouts/TopNav/TopNav';
import NewsBlogName from '../../component/NewsBlogName/NewsBlogName';
import NewsBlogContent from '../../component/Layouts/NewsBlogContent/NewsBlogContent';
import BlogSliderLast from '../../component/Layouts/BlogSliderLast/BlogSliderLast';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import BlogSlideImage1 from '../../assets/imgs/blog/blog-1.png';
import BlogSlideImage2 from '../../assets/imgs/blog/blog-2.png';
import BlogSlideImage3 from '../../assets/imgs/blog/blog-3.png';

import NewsBlogContentTextImage from '../../assets/imgs/news-blog-details-post.jpg';

import AuthorPhoto from '../../assets/imgs/news/author.png';
import FBIcon from '../../assets/imgs/socials/fb.svg';
import TWIcon from '../../assets/imgs/socials/tw.svg';
import INIcon from '../../assets/imgs/socials/in.svg';

const BlogSliderLastData = [
  {
    date: '20th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage1,
    link: '/blogpost',
  },
  {
    date: '21th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
  {
    date: '22th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage3,
    link: '/blogpost',
  },
  {
    date: '23th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
];

const NewsBlogContentText = `
    <p>Rapidiously one-to-one paradigms via performance bametrics. Enthusiastically into bleeding convergence enterprise Enthusiastically  paradigms  per underwhelm. Bleeding convergence enterprise our
    processes compelling collaboration. Professionally revolutionize paradigms  per underwhelm. Bleeding convergence enterprise underwhelm proces compelling collaboration and sharing. Professionally in revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically underwhelm proces Rapidiously whiteboard one-to-one paradigms via performance based metrics. Enthusiastically eng bleeding convergence enterprise paradigms via performance nthusiastically. Bleeding convergence enterprise Enthusiastically underwhelm proces</p>
    <h2>There are no secrets to success. It is the result of preparation, hard work  and learning from failure.
    metrics. Enthusiastically eng bleeding convergence enterprise paradigms</h2>
    <p>Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Professionally revolutionize paradigms via performance underwhelm. Bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Rapidiously whiteboard one to one paradigms via performance based metrics. Professionally revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically</p>
    <img src=${NewsBlogContentTextImage} />
    <p>Rapidiously one-to-one paradigms via performance bametrics. Enthusiastically into bleeding convergence enterprise Enthusiastically  paradigms  per underwhelm. Bleeding convergence enterprise our
    processes compelling collaboration. Professionally revolutionize paradigms  per underwhelm. Bleeding convergence enterprise underwhelm proces compelling collaboration and sharing. Professionally in revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically underwhelm proces Rapidiously whiteboard one-to-one paradigms via performance based metrics. Enthusiastically eng bleeding convergence enterprise paradigms via performance nthusiastically. Bleeding convergence enterprise Enthusiastically underwhelm proces</p>
    <h2>There are no secrets to success. It is the result of preparation, hard work  and learning from failure.
    metrics. Enthusiastically eng bleeding convergence enterprise paradigms</h2>
    <p>Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Professionally revolutionize paradigms via performance underwhelm. Bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Rapidiously whiteboard one to one paradigms via performance based metrics. Professionally revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically</p>
`;
const NewsBlogContentAuthor = {
  name: 'Belinda Gonsales',
  text: 'Rapidiously whiteboard one-to-one paradigms via performance based metrics. Enthusiastically engage bleeding revolutionize convergence enterprise paradigms via performance nthusiastically.',
  photo: AuthorPhoto,
  tags: ['Design', 'App Development', 'Twitter', 'Mobile App Development'],
  links: [
    { linkIcon: FBIcon, linkURL: 'https://fb.com/' },
    { linkIcon: TWIcon, linkURL: 'https://twitter.com/' },
    { linkIcon: INIcon, linkURL: 'https://google.com/' },
  ],
};

function NewsBlogDetails() {
  return (
    <>
      <section className="news-blog-details-banner">
        <div>
          <TopNav isLight />
        </div>
        <div className="page-name-wrapper">
          <NewsBlogName name="With growing from other our beauty use our sources." category="Life Style" time="5" />
        </div>
      </section>

      <section>
        <NewsBlogContent text={NewsBlogContentText} authorInfo={NewsBlogContentAuthor} />
      </section>

      <section className="services-blog">
        <BlogSliderLast blockName="Our Latest Blog" data={BlogSliderLastData} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default NewsBlogDetails;
