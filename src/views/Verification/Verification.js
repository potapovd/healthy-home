import './Verification.css';

import VerificationContent from '../../component/Layouts/Login/VerificationContent';
import TopNav from '../../component/Layouts/TopNav/TopNav';

function Verification() {
  return (
    <>
      <section className="sign-in-bg">
        <div>
          <TopNav whiteBackgroud />
        </div>
        <div className="page-name-wrapper page-name-form">
          <VerificationContent />
        </div>
      </section>
    </>
  );
}

export default Verification;
