import './SignUp.css';

import SignUpContent from '../../component/Layouts/Login/SignUpContent';
import TopNav from '../../component/Layouts/TopNav/TopNav';

function SignUp() {
  return (
    <>
      <section className="sign-in-bg">
        <div>
          <TopNav whiteBackgroud />
        </div>
        <div className="page-name-wrapper page-name-form">
          <SignUpContent />
        </div>
      </section>
    </>
  );
}

export default SignUp;
