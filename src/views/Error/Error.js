import './Error.css';

import ErrorContent from '../../component/Layouts/ErrorContent/ErrorContent';
import TopNav from '../../component/Layouts/TopNav/TopNav';

function Error() {
  return (
    <>
      <section className="error-bg">
        <div>
          <TopNav />
        </div>
        <div className="page-name-wrapper page-name-breadcrumbs">
          <ErrorContent />
        </div>
      </section>
    </>
  );
}

export default Error;
