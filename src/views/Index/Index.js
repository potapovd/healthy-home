import './Index.css';

import ServiceList1 from '../../assets/imgs/services-list/service-1.png';
import ServiceList2 from '../../assets/imgs/services-list/service-2.png';
import ServiceList3 from '../../assets/imgs/services-list/service-3.png';
import ServiceList4 from '../../assets/imgs/services-list/service-4.png';
import ServiceSlideImage1 from '../../assets/imgs/services-slider/service-slide1.png';
import ServiceSlideImage2 from '../../assets/imgs/services-slider/service-slide2.png';
import ServiceSlideImage3 from '../../assets/imgs/services-slider/service-slide3.png';
import ServiceSlideImage4 from '../../assets/imgs/services-slider/service-slide4.png';
import ServiceSlideImage5 from '../../assets/imgs/services-slider/service-slide5.png';
import TopNotification from '../../component/Elements/TopNotification/TopNotification';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import DownloadApplication from '../../component/Layouts/DownloadApplication/DownloadApplication';
import Footer from '../../component/Layouts/Footer/Footer';
import IndexHolicsticHub from '../../component/Layouts/IndexHolicsticHub/IndexHolicsticHub';
import IndexLinks from '../../component/Layouts/IndexLinks/IndexLinks';
import IndexNumbers from '../../component/Layouts/IndexNumbers/IndexNumbers';
import IndexVideo from '../../component/Layouts/IndexVideo/IndexVideo';
import IndexWhyHH from '../../component/Layouts/IndexWhyHH/IndexWhyHH';
import ServicesList from '../../component/Layouts/ServicesList/ServicesList';
import ServicesSlider from '../../component/Layouts/ServicesSlider/ServicesSlider';
import TopBannerIndex from '../../component/Layouts/TopBannerIndex/TopBannerIndex';
import TopNav from '../../component/Layouts/TopNav/TopNav';
import FixedContacts from '../../component/Elements/FixedContacts/FixedContacts';

const ServicesListData = [
  {
    name: 'AC Duct Cleaning',
    text: 'A unique full system AC Duct cleaning process that ensures your indoor air quality is safe for you and your family to breathe and sleep better in your home.',
    image: ServiceList1,
  },
  {
    name: 'Mattress Cleaning',
    text: 'Improve your morning energy levels with our dry and chemical-free mattress cleaning & sanitization. ',
    image: ServiceList2,
  },
  {
    name: 'Safe Pest Control',
    text: 'Traditional and organic safe pest control treatments to get rid of the unwanted pests in your home.',
    image: ServiceList3,
  },
  {
    name: 'Water Tank Cleaning ',
    text: 'Get stronger and healthier skin with our detailed water tank treatment that gives you the purest water quality at home.',
    image: ServiceList4,
  },
];

const ServicesSliderData = [
  {
    name: 'Pure Sleep® Mattress Cleaning',
    text: 'Safe mattress cleaning and sanitization service in Dubai & Abu Dhabi',
    image: ServiceSlideImage1,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Air® AC Duct Cleaning',
    text: 'Best AC duct cleaning and sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage2,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'SPC Program® Safe Pest Control',
    text: 'Green & traditional pest control services in Dubai & Abu Dhabi',
    image: ServiceSlideImage3,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Carpet Cleaning',
    text: 'Premium carpet cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage4,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Sofa Cleaning',
    text: 'Best sofa cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage5,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Carpet Cleaning',
    text: 'Premium carpet cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage4,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Sofa Cleaning',
    text: 'Best sofa cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage5,
    url: '/service',
    booking: '/bookig/ac',
  },
];
const NumbersData = [
  { number: 34000, descr: 'Happy Customers' },
  { number: 87000, descr: 'Mattresses Sanitized' },
  { number: 4.9, descr: 'Google Rating ', isRating: true, ratingMax: 5 },
  { number: 34000, descr: 'Happy Customers ' },
];
const videoData = {
  poster: 'http://localhost:3000/player/poster.jpg',
  sources: [
    {
      src: 'http://localhost:3000/player/video.mp4',
      type: 'video/mp4',
    },
  ],
};
function Index() {
  return (
    <>
      <section className="index-section1">
        <TopNotification />
        <TopNav isLight={false} isLoggined={false} />
        <TopBannerIndex />
      </section>
      <section>
        <IndexNumbers data={NumbersData} length={NumbersData.length} />
      </section>
      <section>
        <ServicesSlider blockName="Explore our services" data={ServicesSliderData} />
      </section>
      <section>
        <IndexWhyHH />
      </section>
      <section>
        <IndexVideo data={videoData} />
      </section>
      <section>
        <IndexHolicsticHub />
      </section>
      <section>
        <ServicesList blockName="Most Popular Services" data={ServicesListData} />
      </section>
      <section>
        <DownloadApplication />
      </section>
      <section>
        <Contacts />
      </section>
      <section>
        <IndexLinks />
      </section>
      <section>
        <Footer />
      </section>
      <section>
        <FixedContacts />
      </section>
    </>
  );
}

export default Index;
