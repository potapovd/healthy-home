import './Bookings.css';

import BookingsContent from '../../component/Layouts/BookingsContent/BookingsContent';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import AddressHomeImage from '../../assets/imgs/addess-home.png';
import AddressWorkImage from '../../assets/imgs/addess-work.png';
import CaledarImage from '../../assets/imgs/calendar-icon.png';

const BookingsList = [
  {
    name: 'Cleaning Services',
    action: 'Modify',
    path: ['AC Cleaning', 'AC Duct cleaning'],
    date: '24 August, 2021',
    price: 'AED 250',
    status: 'Upcoming',
    typesList: ['Appartments', 'Villa', 'Office'],
    typeActive: 'Appartments',
    subTypes: [
      { name: 'Number of Bedrooms', value: 2 },
      { name: 'Number of AC Units/ Controller Remotes', value: 5 },
    ],
    instructions:
      'Assertively maintain clicks and mortar quality vectors are through maintainable sources. Propriately more our Assertively maintain clicks and mortar quality vectors are through maintainable. Assertively maintain clicks and mortar quality vectors are through maintainable sources. Propriately more our Assertively maintain clicks and mortar quality vectors are through maintainable ',
    addressName: 'Home',
    address: '4261 Kembery Drive, Chicago, LSA',
    addressIcon: AddressHomeImage,
    dateTime: '08:00 - 09:00   Tus, 27th Aug 2021',
    dateIcon: CaledarImage,
  },
  {
    name: 'Cleaning Services',
    action: 'Rebook',
    path: ['Sofa Cleaning', 'Deep Vacuuming & Shampooing'],
    date: '24 August, 2021',
    price: 'AED 250',
    status: 'Completed',
    typesList: ['Appartments', 'Villa', 'Office'],
    typeActive: 'Office',
    subTypes: [
      { name: 'Number of Bedrooms', value: '0' },
      { name: 'Number of AC Units/ Controller Remotes', value: 11 },
    ],
    instructions: 'Lorem Ipsum',
    addressName: 'Home',
    address: '4261 Kembery Drive, Chicago, LSA',
    addressIcon: AddressWorkImage,
    dateTime: '09:00 - 10:00  Tus, 21th Aug 2021',
    dateIcon: CaledarImage,
  },
];

function Bookings() {
  return (
    <>
      <section className="booking-section1" />

      <section className="booking-content">
        <BookingsContent data={BookingsList} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Bookings;
