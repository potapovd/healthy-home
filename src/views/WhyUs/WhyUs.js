import './WhyUs.css';

import TopNav from '../../component/Layouts/TopNav/TopNav';
import PageName from '../../component/PageName/PageName';
import BreadCrumbs from '../../component/Elements/BreadCrumbs/BreadCrumbs';
import WhyUsContent from '../../component/Layouts/WhyUsContent/WhyUsContent';
import IndexWhyHH from '../../component/Layouts/IndexWhyHH/IndexWhyHH';
import WhoWeServe from '../../component/Layouts/WhoWeServe/WhoWeServe';
import WhyUsNumbers from '../../component/Layouts/WhyUsNumbers/WhyUsNumbers';
import BlogSliderLast from '../../component/Layouts/BlogSliderLast/BlogSliderLast';
import DownloadApplication from '../../component/Layouts/DownloadApplication/DownloadApplication';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import BlogSlideImage1 from '../../assets/imgs/blog/blog-1.png';
import BlogSlideImage2 from '../../assets/imgs/blog/blog-2.png';
import BlogSlideImage3 from '../../assets/imgs/blog/blog-3.png';

import WhoWeServeImage1 from '../../assets/imgs/who-we-serve/who-we-serve-1.png';
import WhoWeServeImage2 from '../../assets/imgs/who-we-serve/who-we-serve-2.png';
import WhoWeServeImage3 from '../../assets/imgs/who-we-serve/who-we-serve-3.png';
import WhoWeServeImage4 from '../../assets/imgs/who-we-serve/who-we-serve-4.png';
import WhoWeServeImage5 from '../../assets/imgs/who-we-serve/who-we-serve-5.png';
import WhoWeServeImage6 from '../../assets/imgs/who-we-serve/who-we-serve-6.png';
import WhoWeServeImage7 from '../../assets/imgs/who-we-serve/who-we-serve-7.png';
import WhoWeServeImage8 from '../../assets/imgs/who-we-serve/who-we-serve-8.png';

const BlogSliderLastData = [
  {
    date: '20th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage1,
    link: '/blogpost',
  },
  {
    date: '21th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
  {
    date: '22th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage3,
    link: '/blogpost',
  },
  {
    date: '23th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
];

const WhyUsContentText = `
    <h2>There are no secrets to success. It is the result of preparation, hard work  and learning from failure.
    metrics. Enthusiastically eng bleeding convergence enterprise paradigms</h2>

    <p>Rapidiously one-to-one paradigms via performance bametrics. Enthusiastically into bleeding convergence enterprise Enthusiastically  paradigms  per underwhelm. Bleeding convergence enterprise our
    processes compelling collaboration. Professionally revolutionize paradigms  per underwhelm. Bleeding convergence enterprise underwhelm proces compelling collaboration and sharing. Professionally in revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically underwhelm proces Rapidiously whiteboard one-to-one paradigms via performance based metrics. Enthusiastically eng bleeding convergence enterprise paradigms via performance nthusiastically. Bleeding convergence enterprise Enthusiastically underwhelm proces</p>

    <p>Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Professionally revolutionize paradigms via performance underwhelm. Bleeding convergence enterprise Enthusiastically underwhelm processes compelling collaboration and sharing. Rapidiously whiteboard one to one paradigms via performance based metrics. Professionally revolutionize paradigms via performance nthusiastically underwhelm processes. Bleeding convergence enterprise Enthusiastically</p>
`;
const WhoWeServeData = [
  {
    image: WhoWeServeImage1,
    name: 'Healthy Home',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage2,
    name: 'Healthy Hospitality',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage3,
    name: 'Healthy Education',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage4,
    name: 'Healthy Workspace',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage5,
    name: 'Healthy Transportation',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage6,
    name: 'Healthy Clinic',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage7,
    name: 'Healthy Fitness',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
  {
    image: WhoWeServeImage8,
    name: 'Healthy Mosque',
    text: 'Rapidiously whiteboard one to o paradigms via performance based metrics. Enthusiastically  bleeding quality of sleep and beathing',
  },
];

function WhyUs() {
  return (
    <>
      <section className="why-us-banner">
        <div>
          <TopNav isLight />
        </div>
        <div className="page-name-wrapper page-name-breadcrumbs">
          <PageName name="Why Us" />
          <BreadCrumbs
            path={[
              { name: 'home', link: '/' },
              { name: 'Why Us', link: '/why-us' },
            ]}
          />
        </div>
      </section>

      <secton className="why-us-content-text">
        <WhyUsContent text={WhyUsContentText} />
      </secton>

      <section>
        <IndexWhyHH />
      </section>

      <section>
        <WhoWeServe data={WhoWeServeData} />
      </section>

      <section>
        <WhyUsNumbers />
      </section>

      <section>
        <BlogSliderLast blockName="Our Latest Blog" data={BlogSliderLastData} />
      </section>

      <section>
        <DownloadApplication />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default WhyUs;
