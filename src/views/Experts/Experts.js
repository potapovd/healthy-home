import './Experts.css';

import TopNav from '../../component/Layouts/TopNav/TopNav';
import PageName from '../../component/PageName/PageName';
import BreadCrumbs from '../../component/Elements/BreadCrumbs/BreadCrumbs';
import ExpertsPanel from '../../component/Layouts/ExpertsPanel/ExpertsPanel';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

import ExpertPhoto1 from '../../assets/imgs/experts/expert-1.png';
import ExpertPhoto2 from '../../assets/imgs/experts/expert-2.png';
import ExpertPhoto3 from '../../assets/imgs/experts/expert-3.png';
import ExpertPhoto4 from '../../assets/imgs/experts/expert-4.png';

const expertsPanelData = [
  {
    image: ExpertPhoto1,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
  {
    image: ExpertPhoto2,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
  {
    image: ExpertPhoto3,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
  {
    image: ExpertPhoto4,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
  {
    image: ExpertPhoto3,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
  {
    image: ExpertPhoto4,
    name: 'Ashfak Sayem',
    job: 'Designation',
    text: 'Cepteur occaecat cupidatat proident, taken possession entire soul, Asertively maintain click mortar quality vectors through our of next-generation technologies like these sweet mornings of spring which.',
    link: '#',
  },
];

function Experts() {
  return (
    <>
      <section className="experts-section1">
        <div>
          <TopNav isLight />
        </div>
        <div className="page-name-wrapper page-name-breadcrumbs">
          <PageName name="our services" />
          <BreadCrumbs
            path={[
              { name: 'home', link: '/' },
              { name: 'Meet the experts', link: '/experts' },
            ]}
          />
        </div>
      </section>

      <section>
        <ExpertsPanel data={expertsPanelData} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Experts;
