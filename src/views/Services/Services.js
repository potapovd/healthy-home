import './Services.css';

import BlogSlideImage1 from '../../assets/imgs/blog/blog-1.png';
import BlogSlideImage2 from '../../assets/imgs/blog/blog-2.png';
import BlogSlideImage3 from '../../assets/imgs/blog/blog-3.png';
import FeedbackImage from '../../assets/imgs/feedback/img.png';
import serviceAC1 from '../../assets/imgs/services-list/ac-services-1.png';
import serviceAC2 from '../../assets/imgs/services-list/ac-services-2.png';
import serviceCleaningService1 from '../../assets/imgs/services-list/cleaning-services-1.png';
import serviceCleaningService2 from '../../assets/imgs/services-list/cleaning-services-2.png';
import serviceCleaningService3 from '../../assets/imgs/services-list/cleaning-services-3.png';
import serviceDesPest1 from '../../assets/imgs/services-list/disinfection-pest-control-1.png';
import serviceDesPest2 from '../../assets/imgs/services-list/disinfection-pest-control-2.png';
import ServiceSlideImage1 from '../../assets/imgs/services-slider/service-slide1.png';
import ServiceSlideImage2 from '../../assets/imgs/services-slider/service-slide2.png';
import ServiceSlideImage3 from '../../assets/imgs/services-slider/service-slide3.png';
import ServiceSlideImage4 from '../../assets/imgs/services-slider/service-slide4.png';
import ServiceSlideImage5 from '../../assets/imgs/services-slider/service-slide5.png';
import BreadCrumbs from '../../component/Elements/BreadCrumbs/BreadCrumbs';
import BlogSliderLast from '../../component/Layouts/BlogSliderLast/BlogSliderLast';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Feedbacks from '../../component/Layouts/Feedbacks/Feedbacks';
import Footer from '../../component/Layouts/Footer/Footer';
import ServicesList from '../../component/Layouts/ServicesList/ServicesList';
import ServicesSlider from '../../component/Layouts/ServicesSlider/ServicesSlider';
import TopNav from '../../component/Layouts/TopNav/TopNav';
import PageName from '../../component/PageName/PageName';

const ServicesSliderData = [
  {
    name: 'Pure Sleep® Mattress Cleaning',
    text: 'Safe mattress cleaning and sanitization service in Dubai & Abu Dhabi',
    image: ServiceSlideImage1,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Air® AC Duct Cleaning',
    text: 'Best AC duct cleaning and sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage2,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'SPC Program® Safe Pest Control',
    text: 'Green & traditional pest control services in Dubai & Abu Dhabi',
    image: ServiceSlideImage3,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Carpet Cleaning',
    text: 'Premium carpet cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage4,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Sofa Cleaning',
    text: 'Best sofa cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage5,
    url: '/service',
    booking: '/bookig/ac',
  },
];
const ServicesListDataAC = [
  {
    name: 'AC Duct Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceAC1,
  },
  {
    name: 'AC Maintenance',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceAC2,
  },
  {
    name: 'AC Duct Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceAC1,
  },
  {
    name: 'AC Maintenance',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceAC2,
  },
];
const ServicesListDataCleaningService = [
  {
    name: 'AC cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceCleaningService1,
  },
  {
    name: 'Carpets Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceCleaningService2,
  },
  {
    name: 'Mattress Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceCleaningService3,
  },
  {
    name: 'Carpets Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceCleaningService2,
  },
  {
    name: 'Mattress Cleaning',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceCleaningService3,
  },
];
const ServicesListDataesPest = [
  {
    name: 'Pest Control ',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceDesPest1,
  },
  {
    name: 'Disinfection',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceDesPest2,
  },
  {
    name: 'Pest Control ',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceDesPest1,
  },
  {
    name: 'Disinfection',
    text: 'Asertively maintain click and mortar quality vectors through next-generation technologies',
    image: serviceDesPest2,
  },
];
const BlogSliderLastData = [
  {
    date: '20th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage1,
    link: '/blogpost',
  },
  {
    date: '21th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
  {
    date: '22th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage3,
    link: '/blogpost',
  },
  {
    date: '23th June 2021',
    name: 'Private Contemporary Home Balancing Openness',
    image: BlogSlideImage2,
    link: '/blogpost',
  },
];

const FeedbackData = [
  {
    text: 'Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy through',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
  {
    text: 'Assertively maintain clicks and mortar our recaptiualize excellent synergy through',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
  {
    text: 'Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy through Assertively maintain clicks and mortar quality vectors are through and maintainable sources. Propriately more our recaptiualize excellent synergy throug',
    name: 'Cam Sudweeks',
    job: 'Product Designer',
    image: FeedbackImage,
  },
];

function Services() {
  return (
    <>
      <section className="services-section1">
        <div>
          <TopNav isLight />
        </div>
        <div className="page-name-wrapper page-name-breadcrumbs">
          <PageName name="our services" />
          <BreadCrumbs
            path={[
              { name: 'home', link: '/' },
              { name: 'services', link: '/services' },
            ]}
          />
        </div>
      </section>

      <section>
        <ServicesSlider blockName="All Categories" data={ServicesSliderData} />
      </section>

      <section>
        <ServicesList blockName="AC Services" data={ServicesListDataAC} />
        <ServicesList blockName="Cleaning Services" data={ServicesListDataCleaningService} />
        <ServicesList blockName="Disinfection & Pest Control" data={ServicesListDataesPest} />
      </section>

      <section className="services-blog">
        <Feedbacks blockName="What our clients says about our service" data={FeedbackData} />
      </section>

      <section className="services-blog">
        <BlogSliderLast blockName="Our Latest Blog" data={BlogSliderLastData} />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Services;
