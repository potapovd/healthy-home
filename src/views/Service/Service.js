import './Service.css';

import ServiceSlideImage1 from '../../assets/imgs/services-slider/service-slide1.png';
import ServiceSlideImage2 from '../../assets/imgs/services-slider/service-slide2.png';
import ServiceSlideImage3 from '../../assets/imgs/services-slider/service-slide3.png';
import ServiceSlideImage4 from '../../assets/imgs/services-slider/service-slide4.png';
import ServiceSlideImage5 from '../../assets/imgs/services-slider/service-slide5.png';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import DownloadApplication from '../../component/Layouts/DownloadApplication/DownloadApplication';
import Footer from '../../component/Layouts/Footer/Footer';
import ServiceContent from '../../component/Layouts/ServiceContent/ServiceContent';
import ServiceTop from '../../component/Layouts/ServiceTop/ServiceTop';
import ServicesSlider from '../../component/Layouts/ServicesSlider/ServicesSlider';
import TopNav from '../../component/Layouts/TopNav/TopNav';

const ServicesSliderData = [
  {
    name: 'Pure Sleep® Mattress Cleaning',
    text: 'Safe mattress cleaning and sanitization service in Dubai & Abu Dhabi',
    image: ServiceSlideImage1,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Air® AC Duct Cleaning',
    text: 'Best AC duct cleaning and sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage2,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'SPC Program® Safe Pest Control',
    text: 'Green & traditional pest control services in Dubai & Abu Dhabi',
    image: ServiceSlideImage3,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Carpet Cleaning',
    text: 'Premium carpet cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage4,
    url: '/service',
    booking: '/bookig/ac',
  },
  {
    name: 'Pure Living® Sofa Cleaning',
    text: 'Best sofa cleaning & sanitization services in Dubai & Abu Dhabi',
    image: ServiceSlideImage5,
    url: '/service',
    booking: '/bookig/ac',
  },
];

const videoData = {
  poster: 'http://localhost:3000/player/poster.jpg',
  sources: [
    {
      src: 'http://localhost:3000/player/video.mp4',
      type: 'video/mp4',
    },
  ],
};

function Service() {
  return (
    <>
      <section className="service-top">
        <TopNav />
        <ServiceTop videoData={videoData} />
      </section>

      <section className="service-content">
        <ServiceContent />
      </section>

      <section>
        <ServicesSlider blockName="What other services can we help you with?" data={ServicesSliderData} />
      </section>

      <section>
        <DownloadApplication />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Service;
