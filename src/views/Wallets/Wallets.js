import './Wallets.css';

import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';
import WalletsContent from '../../component/Layouts/WalletsContent/WalletsContent';
import TopNav from '../../component/Layouts/TopNav/TopNav';

function Wallets() {
  return (
    <>
      <section className="wallets-section1">
        <TopNav isLight isLoggedIn />
      </section>

      <section className="wallets-content">
        <WalletsContent />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Wallets;
