import React from 'react';
import { Route, Switch } from 'react-router-dom';
import './App.css';
import './App.rtl.css';

import Index from './Index/Index';
import Services from './Services/Services';
import Service from './Service/Service';
import NewsCategories from './NewsCategories/NewsCategories';
import NewsBlogDetails from './NewsBlogDetails/NewsBlogDetails';
import WhyUs from './WhyUs/WhyUs';
import Support from './Support/Support';
import Experts from './Experts/Experts';
import Profile from './Profile/Profile';
import Bookings from './Bookings/Bookings';
import Wallets from './Wallets/Wallets';
import Error from './Error/Error';
import SignIn from './SignIn/SignIn';
import SignUp from './SignUp/SignUp';
import Verification from './Verification/Verification';
import Booking from './Booking/Booking';

function App() {
  return (
    <Switch>
      <Route exact path="/" component={Index} />
      <Route exact path="/services" component={Services} />
      <Route exact path="/service" component={Service} />
      <Route exact path="/news-categories" component={NewsCategories} />
      <Route exact path="/news-blog-details" component={NewsBlogDetails} />
      <Route exact path="/why-us" component={WhyUs} />
      <Route exact path="/support" component={Support} />
      <Route exact path="/experts" component={Experts} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/bookings" component={Bookings} />
      <Route exact path="/wallets" component={Wallets} />

      <Route exact path="/404" component={Error} />
      <Route exact path="/sign-in" component={SignIn} />
      <Route exact path="/sign-up" component={SignUp} />
      <Route exact path="/verification" component={Verification} />

      <Route exact path="/booking/:service" component={Booking} />

      <Route component={Error} />
    </Switch>
  );
}

export default App;
