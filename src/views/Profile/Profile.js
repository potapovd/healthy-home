import './Profile.css';

import TopNav from '../../component/Layouts/TopNav/TopNav';
import PageName from '../../component/PageName/PageName';
import BreadCrumbs from '../../component/Elements/BreadCrumbs/BreadCrumbs';
import ProfileContent from '../../component/Layouts/ProfileContent/ProfileContent';
import Contacts from '../../component/Layouts/Contacts/Contacts';
import Footer from '../../component/Layouts/Footer/Footer';

function Profile() {
  return (
    <>
      <section className="profile-section1">
        <div>
          <TopNav isLight isLoggedIn />
        </div>
        <div className="page-name-wrapper page-name-breadcrumbs">
          <PageName name="Profile" />
          <BreadCrumbs
            path={[
              { name: 'home', link: '/' },
              { name: 'profile', link: '/profile' },
            ]}
          />
        </div>
      </section>

      <section className="profile-content">
        <ProfileContent />
      </section>

      <section>
        <Contacts />
      </section>

      <section>
        <Footer />
      </section>
    </>
  );
}

export default Profile;
