# HEATHY HOME - FRONTEND

- REACTJS
- RTL ready
- Responsive
- 23+ Pages
    *  Slider,
    *  Popups,
    *  Forms,
    *  Multistep Form,
    *  Video Player

DEMO
[https://healthy-home.netlify.app](https://healthy-home.netlify.app)

![Index Page](./readme-src/index.jpg){width=300 height=1150}
![Services Page](./readme-src/services.jpg){width=300 height=715}


## Available Scripts

In the project directory, you can run:

### `yarn install`
### `yarn start`


## Pages
[https://healthy-home.netlify.app/](https://healthy-home.netlify.app/)

[https://healthy-home.netlify.app/services](https://healthy-home.netlify.app/services)

[https://healthy-home.netlify.app/service](https://healthy-home.netlify.app/service)

[https://healthy-home.netlify.app/news-categories](https://healthy-home.netlify.app/news-categories)

[https://healthy-home.netlify.app/news-blog-details](https://healthy-home.netlify.app/news-blog-details)

[https://healthy-home.netlify.app/why-us](https://healthy-home.netlify.app/why-us)

[https://healthy-home.netlify.app/support](https://healthy-home.netlify.app/support)

[https://healthy-home.netlify.app/experts](https://healthy-home.netlify.app/experts)

[https://healthy-home.netlify.app/profile](https://healthy-home.netlify.app/profile)

[https://healthy-home.netlify.app/bookings](https://healthy-home.netlify.app/bookings)

[https://healthy-home.netlify.app/wallets](https://healthy-home.netlify.app/wallets)

[https://healthy-home.netlify.app/sign-in](https://healthy-home.netlify.app/sign-in)

[https://healthy-home.netlify.app/sign-up](https://healthy-home.netlify.app/sign-up)

[https://healthy-home.netlify.app/verification](https://healthy-home.netlify.app/verification)

[https://healthy-home.netlify.app/404](https://healthy-home.netlify.app/404)

[https://healthy-home.netlify.app/booking/ac](https://healthy-home.netlify.app/booking/ac)

[https://healthy-home.netlify.app/booking/carpet](https://healthy-home.netlify.app/booking/carpet)

[https://healthy-home.netlify.app/booking/curtains](https://healthy-home.netlify.app/booking/curtains)

[https://healthy-home.netlify.app/booking/mattress](https://healthy-home.netlify.app/booking/mattress)

[https://healthy-home.netlify.app/booking/sofa](https://healthy-home.netlify.app/booking/sofa)

[https://healthy-home.netlify.app/booking/water-tank](https://healthy-home.netlify.app/booking/water-tank)

[https://healthy-home.netlify.app/booking/disinfection-pest-control](https://healthy-home.netlify.app/booking/disinfection-pest-control)


## Toggle to the RTL
> *src/index.js* -> document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');

## Whole Project
![All Pages](./readme-src/all.jpg){width=400 height=492}
